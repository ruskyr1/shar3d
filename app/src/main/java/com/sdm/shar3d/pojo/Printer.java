package com.sdm.shar3d.pojo;

/**
 * Created by Edgar on 02/04/2017.
 */

public class Printer {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String name;
    String description;
    String image;

}
