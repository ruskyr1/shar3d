package com.sdm.shar3d.pojo;

import java.util.List;


/**
 * Created by Sergio on 22/04/2017.
 */
public class User{

    String username;
    String password;
    boolean designer;
    Printer printer;
    String image;
    String email;
    String imageURL;
    List<String> gallery;
    Double latitude;
    Double longitude;
    Double radius;


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Printer getPrinter() {
        return printer;
    }

    public void setPrinter(Printer printers) {
        this.printer = printers;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }


    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getUrlImage() {
        return image;
    }

    public void setUrlImage(String urlImage) {
        this.image = urlImage;
    }

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.designer = false;
        this.printer = new Printer();
        this.image = "";
        this.imageURL = "";
    }


    public boolean isDesigner() {
        return designer;
    }

    public void setDesigner(boolean designer) {
        this.designer = designer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}


