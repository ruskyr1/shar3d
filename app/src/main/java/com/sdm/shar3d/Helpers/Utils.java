package com.sdm.shar3d.Helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StatFs;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.Persistency.StorageUtils;
import com.sdm.shar3d.Persistency.StorageUtils.StorageInfo;
import com.sdm.shar3d.REST.API;
import com.sdm.shar3d.Services.RequestService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.google.android.gms.internal.zzs.TAG;


public class Utils {



    public static final String PACKAGE_NAME = "com.sdm.shar3d";
    public static final String APIKEY = "78uYSF-3hn2cshyPy8tECye7XcNtf4Ma";
    public static RequestService service;
    static SharedPreferencesHelper session;

    //URL's
    public static String getLoginURL(String user) {
        return "https://api.mlab.com/api/1/databases/sdm/collections/user?q={\"username\":\"" + user + "\"}&fo=false&apiKey=" + APIKEY;
    }

    public static String getUpdateUserURL(String user) {
        return "https://api.mlab.com/api/1/databases/sdm/collections/user?q={\"username\":\"" + user + "\"}&apiKey=" + APIKEY;
    }

    public static String getSignUpURL() {
        return "https://api.mlab.com/api/1/databases/sdm/collections/user?apiKey=" + APIKEY;
    }

    public static String getUpdateProfileURL(String user) {
        return "https://api.mlab.com/api/1/databases/sdm/collections/user?q={\"username\":\"" + user + "\"}&apiKey=" + APIKEY;
    }

    public static String getUpdateLocationURL(double lat, double lon, double radius, String username) {

        String url = getUpdateProfileURL(username);
        return url;
    }

    public static String getUpdateLocationTemplate(double lat, double lon, double radius){
        String template = "{\"$set\":{\"latitude\":" + lat + ", \"longitude\":" + lon + ", \"radius\":" + radius + "}}";
        return template;
    }

    public static String getAdDeleteURL(String id){
        return "https://api.mlab.com/api/1/databases/sdm/collections/anuncios/" + id + "?apiKey=" + APIKEY;
    }


    //FIN URL's


    //CONTENT CHOOSER (CAMERA OR GALLERY)
    public static Intent getMediaChooser(Uri uri) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        camera.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        List<Intent> extraIntents = new ArrayList<Intent>();
        extraIntents.add(camera);
        Intent chooser = Intent.createChooser(intent, "Selecciona una aplicación");

        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents.toArray(new Parcelable[extraIntents.size()]));
        return chooser;
    }

    //GUARDAR UN BITMAP EN UN ARCHIVO
    public static Uri bitmapToFile(Bitmap bitmap, Context context) {
        String imageName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        String currentPath;
        Uri uri = null;
        try {
            image = File.createTempFile(imageName, ".jpeg", storageDir);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (image != null) {
            currentPath = image.getAbsolutePath();
            try {
                FileOutputStream os = new FileOutputStream(image);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


            //session.setImageProfile(currentPath);
            Log.d(TAG, FileProvider.getUriForFile(context, "com.pin.queapproveche", image).toString());
            uri = FileProvider.getUriForFile(context, "com.pin.queapproveche", image);

        }
        return uri;
    }


    //COMPRIMIR BITMAP
    public static Uri compressBitmap(Uri imageUri, int quality, Context context) {
        Bitmap bitmap = null;
        FileOutputStream os = null;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
            os = new FileOutputStream(imageUri.getEncodedPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (bitmap != null) bitmap.compress(Bitmap.CompressFormat.JPEG, quality, os);
        return imageUri;
    }

    //OBTENER LA URI DE UN FILE
    public static Uri createFileUri(Context context) {

        //String imageName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageName = "Imagen";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        String currentPath;
        Uri uri = null;
        try {
            image = File.createTempFile(imageName, ".jpeg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (image != null) {
            uri = Uri.fromFile(image);
        }
        return uri;

    }

    //OBTENER LA IMAGEN DE UN IMAGEVIEW Y CONVERTIRLA EN BASE64

    public static String ViewImageToBase64(ImageView v) {
        Bitmap bitmap;
        bitmap = ((GlideBitmapDrawable) v.getDrawable().getCurrent()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    //CAMBIAR SOLAMENTE LA IMÁGEN DE PERFIL EN LA BD DE UN USUARIO
    public static void changeProfileImage(final Context context, String Base64Image, String username) {

        String template = "{\"$set\":{\"image\":\"" + Base64Image + "\"}}";
        String url = getUpdateProfileURL(username);
        service = RequestService.getInstance(context);

        try {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(template), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(context, "Imagen actualizada correctamente.", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse nr = error.networkResponse;
                    if(nr!= null){
                        Log.d("UTILS", nr.toString());
                    }else{
                        Toast.makeText(context, "Ha habido un error con la red. Inténtalo más tarde.", Toast.LENGTH_LONG).show();
                    }

                }
            });

            service.addToQueue(req);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void updatePrinter(final Context context, final String Base64Image, final String name, final String description, final boolean isDesigner, String username) {

        String template = "{\"$set\":{\"printer.image\":\"" + Base64Image + "\", \"printer.name\":\"" + name + "\", \"printer.description\":\"" + description + "\", \"designer\":\"" + isDesigner + "\"}}";
        String url = getUpdateProfileURL(username);
        service = RequestService.getInstance(context);
        session = new SharedPreferencesHelper(context);
        try {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(template), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    session.setPrinterImage(Base64Image);
                    session.setPrinterName(name);
                    session.setPrinterDescription(description);
                    session.setIsDesigner(isDesigner);
                    Toast.makeText(context, "Impresora actualizada correctamente.", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse nr = error.networkResponse;
                    if(nr!= null){
                        Log.d("UTILS", nr.toString());
                    }else{
                        Toast.makeText(context, "Ha habido un error con la red. Inténtalo más tarde.", Toast.LENGTH_LONG).show();
                    }

                }
            });

            service.addToQueue(req);
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public static void changeUserLocation(final Context context, double lat, double lon, double radius, String username) {

        String template = "{\"$set\":{\"latitude\":\"" + lat + "\", \"longitude\":\"" + lon + "\", \"radius\":\"" + radius + "\"}}";
        String url = getUpdateProfileURL(username);
        service = RequestService.getInstance(context);

        try {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(template), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(context, "Ubicación modificada con éxito.", Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse nr = error.networkResponse;
                    if(nr!= null){
                        Log.d("UTILS", nr.toString());
                    }else{
                        Toast.makeText(context, "Ha habido un error con la red. Inténtalo más tarde.", Toast.LENGTH_LONG).show();
                    }

                }
            });

            service.addToQueue(req);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static String calculateDistance(double lat1, double long1, double lat2, double long2){
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(long1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(long2);

        int distanceInMeters = (int)loc1.distanceTo(loc2);

        return distanceInMeters + " m";
    }


    public static boolean checkEmaill(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static String getCurrentWorkingDirectory(Context _ctx) {
        SharedPreferencesHelper session = new SharedPreferencesHelper(_ctx);
        if (session.getMediaPath().equals("EXTsd") && Utils.getDirectoryExtSD(_ctx, false) != null)
            return Utils.getDirectoryExtSD(_ctx, false);
        else return Environment.getExternalStorageDirectory().toString();
    }


    public static String[] getNacionalidades() {
        String[] isoCountryCodes = Locale.getISOCountries();
        List<String> where = new ArrayList<String>();
        for (String countryCode : isoCountryCodes) {
            Locale locale = new Locale("", countryCode);
            String countryName = locale.getDisplayCountry();
            where.add(countryName);

        }

        String[] array = new String[where.size()];
        where.toArray(array); // fill the array
        return array;


    }

    public static int calcularEdad(int year, int month, int day) {

        Calendar today = Calendar.getInstance();

        int diffYear = today.get(Calendar.YEAR) - year;
        int diffMonth = today.get(Calendar.MONTH) - month;
        int diffDay = today.get(Calendar.DAY_OF_MONTH) - day;
        // Si est? en ese a?o pero todav?a no los ha cumplido
        if (diffMonth < 0 || (diffMonth == 0 && diffDay < 0)) {
            diffYear = diffYear - 1; // no aparec?an los dos guiones del
        }

        return diffYear;

    }

    public static String findMD5(String arg) {

        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException nsae) {
            System.out.println(nsae.getMessage());
        }
        byte[] defaultBytes = arg.getBytes();
        algorithm.reset();
        algorithm.update(defaultBytes);
        byte messageDigest[] = algorithm.digest();
        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xff & messageDigest[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }


    public static int getFontSize(Activity activity) {

        DisplayMetrics dMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);

        // lets try to get them back a font size realtive to the pixel width of the screen
        final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
        int valueWide = (int) (WIDE / 32.0f / (dMetrics.scaledDensity));
        return valueWide;
    }


    //DEVUELVE EL BITMAP REDIMENSIONADO
    public static Bitmap decodeSampledBitmapFromResource(String res, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(res, options);

        // Calculate inSampleSize
        options.inSampleSize = Utils.calculateInSampleSizeInside(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(res, options);
    }

    public static Bitmap decodeSampledBitmapFromDrawable(Context activity, String res, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //System.out.println("esta3->" + res + "/" + activity.getResources().getIdentifier(res, "drawable", "com.example.docaper" ));

        BitmapFactory.decodeResource(activity.getResources(), activity.getResources().getIdentifier(res, "drawable", activity.getPackageName()), options);
        //System.out.println("despues");
        options.inSampleSize = Utils.calculateInSampleSizeInside(options, reqWidth, reqHeight);
        //System.out.println(options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inScaled = false;
        return BitmapFactory.decodeResource(activity.getResources(), activity.getResources().getIdentifier(res, "drawable", activity.getPackageName()), options);
    }


    //DEVUELVE LA ESCALA A CALCULAR PARA REDIMENSIONAR IMAGENES
//	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//		// Raw height and width of image
//		final int height = options.outHeight;
//		final int width = options.outWidth;
//		System.out.println(width+"/"+height +"->" + reqWidth+"/"+reqHeight);
//		int inSampleSize = 1;
//
//		if (height > reqHeight || width > reqWidth) {
//
//		    final int halfHeight = height / 2;
//		    final int halfWidth = width / 2;
//
//		    // Calculate the largest inSampleSize value that is a power of 2 and keeps both
//		    // height and width larger than the requested height and width.
//		    while ((halfHeight / inSampleSize) > reqHeight
//		            && (halfWidth / inSampleSize) > reqWidth) {
//		        inSampleSize *= 2;
//		    }
//		}
//
//		return inSampleSize;
//		}

    public static int calculateInSampleSizeInside(BitmapFactory.Options options, int maxWidth, int maxHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        //System.out.println(width+"/"+height +"->" + maxWidth+"/"+maxHeight);
        if (height > maxHeight || width > maxWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) maxHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) maxWidth);
            }

            if (height < maxHeight && width > maxWidth) {
                inSampleSize = Math.round((float) width / (float) maxWidth);
            }

            if (height > maxHeight && width < maxWidth) {
                inSampleSize = Math.round((float) height / (float) maxHeight);
            }

            if (inSampleSize == 1) {
                inSampleSize += 1;
            }
        }
        //System.out.println(inSampleSize);
        return inSampleSize;
    }


    //DEVUELVE EL TAMA?O(ANCHO) EFECTIVO DE LA PANTALLA
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getDeviceWidth(Activity activity) {
        int deviceWidth = 0;

        Point size = new Point();
        WindowManager windowManager = activity.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.getDefaultDisplay().getSize(size);
            deviceWidth = size.x;
        } else {
            Display display = windowManager.getDefaultDisplay();
            deviceWidth = display.getWidth();
        }
        return deviceWidth;
    }


    //DEVUELVE EL TAMA?O(ALTO) EFECTIVO DE LA PANTALLA
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getDeviceHeight(Activity activity) {
        int deviceHeight = 0;

        Point size = new Point();
        WindowManager windowManager = activity.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.getDefaultDisplay().getSize(size);
            deviceHeight = size.y;
        } else {
            Display display = windowManager.getDefaultDisplay();
            deviceHeight = display.getHeight();
        }
        return deviceHeight;
    }


    //COMPRUEBA SI HAY SALIDA A INTERNET
    public static Boolean isInternetConn(Context ctx) {

        ConnectivityManager connec = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        boolean conectado = false;
        if ((wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected())) {

            try {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                URL myurl = new URL("http://www.google.com");
                URLConnection connection;
                connection = myurl.openConnection();
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                int responseCode = -1;
                responseCode = httpConnection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    conectado = true;
                    httpConnection.disconnect();
                } else {
                    httpConnection.disconnect();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return conectado;
    }


    //COMPRUEBA SI EXISTE MEMORIA EXTERNA
    public static Boolean isSD() {

        String state = Environment.getExternalStorageState();
//		System.out.println("++++++++++++++" + System.getenv("EXTERNAL_STORAGE") + "++" + System.getenv("SECONDARY_STORAGE") + "++" + System.getenv("EMULATED_STORAGE_TARGET") + "++" + Environment.getStorageState(new File(System.getenv("SECONDARY_STORAGE"))) + "++" + Environment.getStorageState(new File(System.getenv("EMULATED_STORAGE_TARGET") + "/0")) + "++" +  Environment.getStorageState(new File(System.getenv("EXTERNAL_STORAGE"))));
        Boolean estado = false;
        if (Environment.MEDIA_MOUNTED.equals(state) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can read and write the media
            estado = true;
        }
        return estado;
    }


    public static String getDirectoryExtSD(Context ctx, boolean isPicker) {
        //System.out.println("ENTRO EN DIRECTORY SD " + Build.VERSION.SDK_INT);
        List<StorageInfo> directorios = StorageUtils.getStorageList();
        String directorio = null;

        for (int s = 0; s < directorios.size(); s++) {
            //System.out.println(directorios.get(s).path);
            if (!directorios.get(s).internal && !directorios.get(s).readonly) {

                //System.out.println("no es internal ni de lectura");

                if ((Build.VERSION.SDK_INT >= 19) && !isPicker) {
                    //>4.4
                    File[] lista = ctx.getExternalFilesDirs(null);
                    //System.out.println("Directorios de lista " + lista.length);
                    for (int d = 0; d < lista.length; d++) {
                        //System.out.println("direcotrios 4.4 " + lista[d].toString() + " -> " + directorios.get(s).path + " contains " + lista[d].getAbsolutePath().contains(directorios.get(s).path));
                        if (lista[d].getAbsolutePath().contains(directorios.get(s).path)) {

                            directorio = lista[d].toString();
                        }
                    }
                    if (directorio == null) {
                        File directorioRest = new File(directorios.get(s).path + "/Android/data/" + ctx.getPackageName() + "/files");
                        if (!directorioRest.exists()) directorioRest.mkdirs();
                        directorio = directorios.get(s).path + "/Android/data/" + ctx.getPackageName() + "/files";
                    }
                } else {
                    directorio = directorios.get(s).path;
                }
            }
        }
        //System.out.println("FF " + directorio);
        return directorio;
    }

    //COMPRUEBA SI TODOS LOS CARACTERES CON NUMERICOS
    public static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    //DEVUELVE EL NOMBRE UNICO DEL DISPOSITIVO JUNTO CON EL ULTIMO BYTE DE MAC

    @SuppressWarnings("static-access")
    public static String getDeviceName(Context ctx, boolean mac) {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        String macAddress = null;
        WifiManager wifiManager = (WifiManager) ctx.getSystemService(ctx.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        macAddress = wInfo.getMacAddress();
        String[] MacBytes = macAddress.split(":");
        String lastMacByte = MacBytes[5];

        if (model.startsWith(manufacturer)) {
            if (mac)
                return capitalize(model + "-" + lastMacByte);
            else return capitalize(model);

        } else {
            if (mac)
                return capitalize(manufacturer) + " " + model + "-" + lastMacByte;
            else return capitalize(manufacturer) + " " + model;
        }
    }


    //TRANSFORMA A MAYUSCULAS LA PRIMERA LETRA DE UN STRING
    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    //DEVUELVE UN LISTADO CON LOS ARCHIVOS DE UN DIR
    public static List<File> listf(List<File> archivos, File dir) {

        if (!dir.isDirectory()) {
            archivos.add(dir);
            return archivos;
        }
        for (File file : dir.listFiles())
            listf(archivos, file);
        return archivos;
    }


    //ELIMINA DIRECTORIO/ARCHIVO
    public static boolean delete(File file) throws IOException {
        boolean borrado = false;
        if (file.isDirectory()) {
            if (file.list().length == 0) {
                borrado = file.delete();
            } else {

                String files[] = file.list();
                for (String temp : files) {

                    File fileDelete = new File(file, temp);
                    delete(fileDelete);
                }

                if (file.list().length == 0) borrado = file.delete();

            }

        } else {

            borrado = file.delete();

        }

        return borrado;
    }


    //DEVUELVE EL TAMA?O EN BYTES DE UN DIRECTORIO
    public static long dirSize(File dir) {

        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += dirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }


    //DEVUELVE EL TAMA?O DISPONIBLE EN EL ALMACENAMIENTO EXTERNO
    @SuppressWarnings("deprecation")
    public static long remainingExternalSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = 0;
        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
        return bytesAvailable;
    }

    //DEVUELVE EL TAMA?O DISPONIBLE EN EL ALMACENAMIENTO EXTERNO SD
    @SuppressWarnings("deprecation")
    public static long remainingExternalSDSpace(Context ctx) {
        StatFs stat = new StatFs(getDirectoryExtSD(ctx, true));
        long bytesAvailable = 0;
        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
        return bytesAvailable;
    }

    @SuppressWarnings("deprecation")
    public static long totalExternalSDSpace(Context ctx) {
        StatFs stat = new StatFs(getDirectoryExtSD(ctx, false));
        long bytesAvailable = 0;

        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getBlockCount() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getBlockCountLong() * stat.getBlockSizeLong();

        return bytesAvailable;
    }


    @SuppressWarnings("deprecation")
    public static long totalExternalSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long bytesAvailable = 0;
        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getBlockCount() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getBlockCountLong() * stat.getBlockSizeLong();
        return bytesAvailable;
    }

    public static double getPercentAvailable() {
        double tExternal = (double) ((double) ((double) ((double) totalExternalSpace() / 1024) / 1024) / 1024);
        double aExternal = (double) ((double) ((double) ((double) remainingExternalSpace() / 1024) / 1024) / 1024);

        return (aExternal * 100) / tExternal;
    }

    public static double getPercentAvailableSD(Context ctx) {
        double texternalSD = (double) ((double) ((double) ((double) totalExternalSDSpace(ctx) / 1024) / 1024) / 1024);
        double aExternalSD = (double) ((double) ((double) ((double) remainingExternalSDSpace(ctx) / 1024) / 1024) / 1024);

        return (aExternalSD * 100) / texternalSD;
    }

    //DEVUELVE EL TAMA?O DISPONIBLE EN EL ALMACENAMIENTO INTERNO
    @SuppressWarnings("deprecation")
    public static long remainingInternalSpace() {

        StatFs stat = new StatFs(Environment.getDataDirectory().getAbsolutePath());
        long bytesAvailable = 0;
        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
        return bytesAvailable;
    }

    @SuppressWarnings("deprecation")
    public static long totalInternalSpace() {

        StatFs stat = new StatFs(Environment.getDataDirectory().getAbsolutePath());
        long bytesAvailable = 0;
        if (Build.VERSION.SDK_INT < 18)
            bytesAvailable = (long) stat.getBlockCount() * (long) stat.getBlockSize();
        else bytesAvailable = stat.getBlockCountLong() * stat.getBlockSizeLong();
        return bytesAvailable;
    }

}
