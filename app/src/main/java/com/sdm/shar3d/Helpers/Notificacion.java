package com.sdm.shar3d.Helpers;

import java.util.Map;



public class Notificacion {
    String titulo,cuerpo,icon;
    Map<String,String> data;

    public Notificacion(String titulo, String cuerpo, String icon, Map<String, String> data) {
        this.titulo = titulo;
        this.cuerpo = cuerpo;
        this.icon = icon;
        this.data = data;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitulo() {
        return titulo;

    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }


    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}

