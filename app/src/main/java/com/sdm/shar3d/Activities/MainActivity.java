package com.sdm.shar3d.Activities;






import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sdm.shar3d.Fragments.FragmentAdminNotifications;
import com.sdm.shar3d.Fragments.FragmentAnuncios;
import com.sdm.shar3d.Fragments.FragmentLogin;
import com.sdm.shar3d.Fragments.FragmentProfile;
import com.sdm.shar3d.Fragments.FragmentProfilePrinter;
import com.sdm.shar3d.Helpers.CircleTransform;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;

import java.util.List;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String currentPosition = "FragmentAnuncios";
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View navHeader;
    SharedPreferencesHelper sesion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        //boton desplegable
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //carga datos usuario
        sesion = new SharedPreferencesHelper(this);
        navHeader = navigationView.getHeaderView(0);


        loadNavHeader();


        if (savedInstanceState != null){
            currentPosition = savedInstanceState.getString("currentPos");
            System.out.println("SAVEDINSTANCE MAIN !NULL " + currentPosition);
        }


        if(savedInstanceState == null)
            setFragment(getFragment("FragmentAnuncios"));



        navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(sesion.isLoggedIn()){ setFragment(new FragmentProfile());}
                if(sesion.isLoggedIn()){
                    if(sesion.getIsPrinter())
                        setFragment(new FragmentProfilePrinter());
                    else
                        setFragment(new FragmentProfile());
                }
                else{setFragment(getFragment("FragmentLogin"));}
                //Comprobar si tiene impresora o no.
            }
        });


    }



    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //guarda estado de la app al minimizar
        outState.putString("currentPos", currentPosition);
        super.onSaveInstanceState(outState);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // maneja eventos de los elementos del menu al pulsarlos
        int id = item.getItemId();

        Fragment fragment = null;
        if (id == R.id.nav_manage) {
            if(sesion.isLoggedIn()){
                if(!sesion.getIsPrinter())
                    fragment = getFragment("FragmentProfile");
                else
                    fragment = new FragmentProfilePrinter();
            }
            else fragment = getFragment("FragmentLogin");

        } else if (id == R.id.nav_anuncios) {
            fragment = getFragment("FragmentAnuncios");
        } /*else if (id == R.id.nav_notifications) {
            fragment = getFragment("FragmentAdminNotifications");
        }*/

        setFragment(fragment);


        return true;
    }

    public void loadNavHeader() {


        TextView nick = (TextView) navHeader.findViewById(R.id.nickProfile);
        TextView email = (TextView) navHeader.findViewById(R.id.emailProfile);
        ImageView imagen = (ImageView) navHeader.findViewById(R.id.imageProfile);

        if(sesion.isLoggedIn()){
            System.out.println(sesion.getUser());
            nick.setText(sesion.getUser());
            email.setText(sesion.getEmail());

            Glide.with(this).load(Base64.decode(sesion.getImageProfile(), Base64.DEFAULT))
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imagen);
        }

        else{

            nick.setText("Invitado");
            email.setText("Identificate o registrate para obtener ventajas!");

            Glide.with(this).load(R.drawable.ic_face_white_48dp)
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imagen);
        }


    }


    public Fragment getFragment(String _clase){
        //lee los fragments ya creados en memoria
        List<Fragment> frgmnts = getSupportFragmentManager().getFragments();
        Fragment result = null;
        //si ya se ha creado previamente lo recupero
        if (frgmnts != null)System.out.println("NUMERO DE FRAGMENTS EN MANAGER "+ frgmnts.size() + " --> " + frgmnts);

        if (frgmnts != null && !frgmnts.isEmpty())
            for (Fragment f : frgmnts) {
                //si el fragment ya esta memoria lo obtengo
                if (f != null && f.getClass().getSimpleName().equals(_clase)) {
                    System.out.println("current frgment " + f.getClass().getSimpleName() + "/" + _clase);
                    result = f;
                }
            }
        //si no se ha creado previamente lo creo
        System.out.println("GET FRAGMENT ES NULL EN MANAGER");
        if (result == null) {
            switch (_clase) {

                case "FragmentAnuncios":
                    result = new FragmentAnuncios();
                    break;

                case "FragmentProfile":
                    result = new FragmentProfile();
                    break;

                case "FragmentLogin":
                    result = new FragmentLogin();
                    break;


                case "FragmentAdminNotifications":
                    result = new FragmentAdminNotifications();
                    break;

            }

        }


        return result;
    }
    //Lanzo el fragment a memoria
    public void setFragment(Fragment _fragment){

        //se usa el getSupportFragmentManager porque se esta utilizando los fragments del paquete de soporte v4
        FragmentManager fManager = getSupportFragmentManager();

        FragmentTransaction ft = fManager.beginTransaction();
        //reemplazo el posible fragment actual con el seleccionado en el layout correspondiente
        ft.replace(R.id.content_frame, _fragment);
        //es obligatorio para que se genere el fragment
        ft.commit();
        currentPosition = _fragment.getClass().getSimpleName();
        setTitle(currentPosition);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void setTitle(String father) {
        switch (father) {

            case "FragmentProfile":
                getSupportActionBar().setTitle("Mi perfil");
                break;

            case "FragmentLogin":
                getSupportActionBar().setTitle("Inicio de sesión");
                break;

            case "FragmentAnuncios":
                getSupportActionBar().setTitle("Anuncios");
                break;

            case "FragmentAdminNotifications":
                getSupportActionBar().setTitle("Administrador de notificaciones");
                break;
        }
    }

}