package com.sdm.shar3d.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;
import com.sdm.shar3d.pojo.User;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfilePicture extends Fragment {

    private static final int MEDIA_CODE = 1;
    GsonBuilder gb;
    Gson gson;
    User user;
    private View topView;
    private FragmentActivity parent;
    private String TAG = "FragmentProfilePicture";
    RequestService service;
    Uri imageUri;
    ProgressDialog pd;


    ImageView profilePicture;
    Button buttonStart;


    public FragmentProfilePicture() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_profile_picture, container, false);
        parent = getActivity();

        service = RequestService.getInstance(parent);




        gb = new GsonBuilder().serializeNulls();
        gson = gb.create();
        if (getArguments() != null) {
            user = gson.fromJson(getArguments().getString("user"), User.class);
            Log.d(TAG, user.toString());
        }

        AppBarLayout appBarLayout = (AppBarLayout) parent.findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);

        profilePicture = (ImageView)topView.findViewById(R.id.imageProfileSignUp);
        buttonStart = (Button) topView.findViewById(R.id.buttonNext);



        //Listeners

        //CAMBIAR LA IMÁGEN DE PERFIL
        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageUri = Utils.createFileUri(getActivity());
                Intent chooser = Utils.getMediaChooser(imageUri);
                startActivityForResult(chooser, MEDIA_CODE);
            }
        });

        //TERMINAR EL PROCESO DE REGISTRO
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd = new ProgressDialog(parent);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Espera por favor");
                pd.show();

                if(profilePicture.getDrawable() != null){
                    Bitmap bitmap = null;
                    String bitmap64 = null;
                    try{
                        bitmap = ((GlideBitmapDrawable) profilePicture.getDrawable().getCurrent()).getBitmap();
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        bitmap64 = Base64.encodeToString(byteArray, Base64.DEFAULT);


                    }catch (ClassCastException e){
                        e.printStackTrace();
                    }


                    user.setImage(bitmap64);
                }
                final String jsonUser = gson.toJson(user);

                try {
                    JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, Utils.getUpdateProfileURL(user.getUsername()), new JSONObject(jsonUser), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());

                            Fragment nextFrag = new FragmentSetLocation();
                            Bundle args = new Bundle();
                            args.putString("user", user.getUsername());
                            nextFrag.setArguments(args);

                            if(pd.isShowing())
                                pd.dismiss();

                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, nextFrag)
                                    .commit();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if (response != null) {
                                String json = new String(response.data);
                                Log.d(TAG, json);
                            } else {
                                Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                            }
                            if(pd.isShowing())
                                pd.dismiss();
                        }
                    });
                    RequestService.getInstance(getContext()).addToQueue(req);
                } catch (Exception a) {
                    if(pd.isShowing())
                        pd.dismiss();
                    a.printStackTrace();
                }
            }
        });


        return topView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_CODE && resultCode == RESULT_OK) {
            Log.d(TAG, "Imagen cambiada!!!!!");
            if (null != data && data.getDataString() != null) {
                Log.d(TAG, "GALERIA");
                String uri = data.getDataString();
                Glide.with(this).load(uri)
                        .crossFade()
                        //.thumbnail(1f)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profilePicture);
            } else {
                Log.d(TAG, "CAMARA");
                Utils.compressBitmap(imageUri, 10, parent);
                Glide.with(this).load(imageUri)
                        .crossFade()
                        //.thumbnail(1f)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profilePicture);
            }
        }
    }

}
