package com.sdm.shar3d.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Helpers.CircleTransform;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.pojo.User;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfilePrinterInfo extends Fragment {

    public static final String TAG = "ProfilePrinterInfo";

    String printerName;
    String printerDescription;
    String printerImage;
    boolean isDesigner;

    EditText printerNameTV, printerDescriptionTV;
    CheckedTextView isDesignerTV;
    ImageView printerImageIV;
    Button saveChanges;
    ImageButton imageButtonName, imageButtonDescription;

    View topView;
    Activity parent;

    Uri imageUri;

    SharedPreferencesHelper session;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        topView = inflater.inflate(R.layout.fragment_profile_printer_info, container, false);
        parent = getActivity();

        session = new SharedPreferencesHelper(parent);

        printerNameTV = (EditText) topView.findViewById(R.id.printerName);
        printerDescriptionTV = (EditText) topView.findViewById(R.id.printerDescription);
        isDesignerTV = (CheckedTextView) topView.findViewById(R.id.checkedTextViewIsDesigner);
        printerImageIV = (ImageView) topView.findViewById(R.id.imagePrinter);
        saveChanges = (Button) topView.findViewById(R.id.buttonNext);
        imageButtonName = (ImageButton) topView.findViewById(R.id.flechaEditName);
        imageButtonDescription = (ImageButton) topView.findViewById(R.id.flechaEditDescription);

        printerNameTV.setInputType(InputType.TYPE_NULL);
        printerNameTV.setFocusable(false);
        printerNameTV.setFocusableInTouchMode(false);
        printerNameTV.setClickable(false);
        printerDescriptionTV.setInputType(InputType.TYPE_NULL);
        printerDescriptionTV.setFocusable(false);
        printerDescriptionTV.setFocusableInTouchMode(false);
        printerDescriptionTV.setClickable(false);

        if (getArguments() != null) {
            //Obtiene los argumentos
            String profileUserJson = getArguments().getString("user");
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            User user = gson.fromJson(profileUserJson, User.class );
            printerName = user.getPrinter().getName();
            printerDescription = user.getPrinter().getDescription();
            printerImage = user.getPrinter().getImage();
            isDesigner = user.isDesigner();

            saveChanges.setVisibility(View.GONE);
        } else {
            printerName = session.getPrinterName();
            printerDescription = session.getPrinterDescription();
            printerImage = session.getPrinterImage();
            isDesigner = session.getIsDesigner();

            imageButtonName.setVisibility(View.VISIBLE);
            imageButtonDescription.setVisibility(View.VISIBLE);

            isDesignerTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckedTextView) v).isChecked()){
                        ((CheckedTextView) v).setChecked(false);
                    }else
                        ((CheckedTextView) v).setChecked(true);

                }
            });

            //Listeners si el perfil es propio

            printerImageIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageUri = Utils.createFileUri(parent);
                    Intent chooser = Utils.getMediaChooser(imageUri);
                    startActivityForResult(chooser, 1);
                }
            });

            saveChanges.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Coge la imágen actual del ImageView, la transforma a base64 y la mete en session.
                    String base64image;
                    try {
                        Bitmap bitmap = ((GlideBitmapDrawable) printerImageIV.getDrawable().getCurrent()).getBitmap();
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        base64image = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        session.setPrinterImage(base64image);

                        Utils.updatePrinter(parent, base64image, printerNameTV.getText().toString(),
                                printerDescriptionTV.getText().toString(), isDesignerTV.isChecked(), session.getUser());



                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }

                }
            });

            imageButtonName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    printerNameTV.setFocusable(true);
                    printerNameTV.setFocusableInTouchMode(true);
                    printerNameTV.setClickable(true);
                    printerNameTV.setInputType(InputType.TYPE_CLASS_TEXT);
                    imageButtonName.setVisibility(View.GONE);
                    printerNameTV.requestFocus();
                }
            });
            imageButtonDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    printerDescriptionTV.setFocusable(true);
                    printerDescriptionTV.setFocusableInTouchMode(true);
                    printerDescriptionTV.setClickable(true);
                    printerDescriptionTV.setInputType(InputType.TYPE_CLASS_TEXT);
                    imageButtonDescription.setVisibility(View.GONE);
                    printerDescriptionTV.requestFocus();
                }
            });
        }

        try {
            printerNameTV.setText(printerName);
            printerDescriptionTV.setText(printerDescription);
            isDesignerTV.setChecked(isDesigner);

            Glide.with(this).load(Base64.decode(printerImage, Base64.DEFAULT))
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(parent))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(printerImageIV);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Listeners


        return topView;

    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Log.d("FragmentProfileOnRespon", String.valueOf(reqCode));
        if (resCode == RESULT_OK) {
            if (reqCode == 1) {


                Log.d(TAG, "Imagen impresora cambiada!!!!!");
                printerImageIV.getDrawable();

                if (data != null && null != data.getDataString()) {
                    Log.d(TAG, "GALERIA");
                    String uri = data.getDataString();
                    //session.setImageProfile(uri);
                    //contactImageImgView.setImageURI(data.getData());
                    Glide.with(this).load(uri)
                            .crossFade()
                            //.thumbnail(0.5f)
                            .bitmapTransform(new CircleTransform(parent))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(printerImageIV);
                } else {
                    Log.d(TAG, "CAMARA");
                    //Bundle extras = data.getExtras();
                    //Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                    //Uri uri = Utils.bitmapToFile(imageBitmap, parent);
                    //session.setImageProfile(uri.toString());
                    Utils.compressBitmap(imageUri, 10, parent);

                    //contactImageImgView.setImageBitmap(bitmap);
                    //session.setImageProfile(imageUri.getEncodedPath());
                    Glide.with(this).load(imageUri)
                            .crossFade()
                            //.thumbnail(0.5f)
                            .bitmapTransform(new CircleTransform(parent))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(printerImageIV);
                }
            }
        }


    }
}