package com.sdm.shar3d.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.sdm.shar3d.Activities.MainActivity;
import com.sdm.shar3d.Adapters.AnunciosListAdapter;
import com.sdm.shar3d.Helpers.DividerItemDecoration;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.REST.Anuncio;
import com.sdm.shar3d.REST.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentAnuncios extends Fragment {


    public static final String ACTION_NOTIFY_NEW_ANUNCIO = "newAnuncio";
    private RecyclerView list;
    private AnunciosListAdapter adapter;
    private ArrayList<Anuncio> anuncios;
    private View topView;
    private boolean needToUpdate;

    private boolean isRequestRunning = false;
    private FloatingActionButton crearAnuncio;
    private Call<List<Anuncio>> apiRequest;
    private String TAG = "FragmentAnuncios";
    private int filtroActual = 0;


    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private SharedPreferencesHelper session;


    private SwipeRefreshLayout swipeContainer;


    private final BroadcastReceiver mYourBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Anuncio receivedAnuncio = new Gson().fromJson(intent.getStringExtra("anuncio"), Anuncio.class);
            anuncios.add(0, receivedAnuncio);
            adapter.notifyItemInserted(0);
            list.scrollToPosition(0);

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mYourBroadcastReceiver,
                new IntentFilter(ACTION_NOTIFY_NEW_ANUNCIO));
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Anuncios");

    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mYourBroadcastReceiver);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //cuando se crea la vista
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_list_anuncios, container, false);
        //puntero al controlador de la lista
        list = (RecyclerView) topView.findViewById(R.id.list);

        session = new SharedPreferencesHelper(getActivity());

        //menu de la action bar
        setHasOptionsMenu(true);
        //Muestro toolbar cuando entro en un Fragment
        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);


        //TabLayout
        TabLayout tabLayout = (TabLayout) topView.findViewById(R.id.toolbarfilter);
        tabLayout.addTab(tabLayout.newTab().setText("General"));
        if (session.isLoggedIn())
            tabLayout.addTab(tabLayout.newTab().setText("Mis anuncios"));
        //si esta logueado tiene otra tab mas e incluye funcionalidad del floating Button
        if (session.isLoggedIn()) {
            session = new SharedPreferencesHelper(getActivity());
            crearAnuncio = (FloatingActionButton) topView.findViewById(R.id.floatingCrear);
            if (session.getIsPrinter())
                crearAnuncio.setVisibility(View.VISIBLE);
            crearAnuncio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment fragmentCrear = new FragmentCrearAnuncio();

                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, fragmentCrear);
                    ft.commit();
                    ((MainActivity) getActivity()).getSupportActionBar().setTitle("Crear Anuncio");

                }
            });
        }


        // inicializacion pull to refresh
        swipeContainer = (SwipeRefreshLayout) topView.findViewById(R.id.swipeContainer);

        //reinicio datos
        if (savedInstanceState != null) {
            System.out.println("no es nul al entrar con " + savedInstanceState.getInt("tab"));
            filtroActual = savedInstanceState.getInt("tab");


            if (!savedInstanceState.getBoolean("asyncTask")) {
                System.out.println("no tiene async");
                anuncios = savedInstanceState.getParcelableArrayList("anuncios");
            } else getAnuncios(true, filtroActual);

        } else {
            System.out.println("es nul al entrar  ");
            anuncios = new ArrayList<>();
            getAnuncios(true, filtroActual);
        }
        tabLayout.getTabAt(filtroActual).select();

        adapter = new AnunciosListAdapter(getActivity(), anuncios);

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment fragmentAmpli = new FragmentAnuncioAmp();


                Bundle bundle = new Bundle();
                bundle.putString("nombre", anuncios.get(list.getChildLayoutPosition(v)).getUsername());
                bundle.putString("descripcion", anuncios.get(list.getChildLayoutPosition(v)).getDescripcion());
                bundle.putString("zona", anuncios.get(list.getChildLayoutPosition(v)).getZona());
                bundle.putString("userImg", anuncios.get(list.getChildLayoutPosition(v)).getImgUser());
                bundle.putString("contacto", anuncios.get(list.getChildLayoutPosition(v)).getContacto());
                bundle.putString("precio", anuncios.get(list.getChildLayoutPosition(v)).getPrecio());
                bundle.putString("printer", anuncios.get(list.getChildLayoutPosition(v)).getPrintername());
                bundle.putString("printerImg", anuncios.get(list.getChildLayoutPosition(v)).getImgPrinter());
                bundle.putString("printerDesc", anuncios.get(list.getChildLayoutPosition(v)).getDescPrinter());
                bundle.putString("id", anuncios.get(list.getChildLayoutPosition(v)).get_id());
                fragmentAmpli.setArguments(bundle);

                // Add Fragment B
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragmentAmpli);
                ft.commit();
                ((MainActivity) getActivity()).getSupportActionBar().setTitle(anuncios.get(list.getChildLayoutPosition(v)).getPrintername());


            }
        });


        //configuro adaptador de la lista
        list.setAdapter(adapter);


        //decoracion lista
        list.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        list.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        list.setItemAnimator(new DefaultItemAnimator());


        // Setup refresh listener which triggers new data loading

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAnuncios(false, filtroActual);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        switch (tab.getPosition()) {
                            case 0:

                                getAnuncios(true, 0);

                                break;
                            case 1:
                                getAnuncios(true, 1);

                                break;
                            case 2:
                                getAnuncios(true, 2);
                                break;
                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        // ...
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        // ...
                    }
                }
        );


        return topView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        if (apiRequest != null && isRequestRunning) {
            apiRequest.cancel();
            outState.putBoolean("asyncTask", true);
        } else outState.putBoolean("asyncTask", false);
        System.out.println("entro a saved en " + filtroActual);
        outState.putInt("tab", filtroActual);
        outState.putParcelableArrayList("anuncios", anuncios);
        outState.putParcelable("anunciosSeleccionados", adapter.getmStatusTaskSelected());
        super.onSaveInstanceState(outState);
    }


    //////////////////////// AUX METHODS

    public boolean esMiAnuncio(Anuncio anun) {
        return session.getUser().equals(anun.getUsername());
    }

    public void filtrarMisAnuncios() {
        ArrayList<Anuncio> mios = new ArrayList<>();

        for (Anuncio current : anuncios) {
            if (esMiAnuncio(current)) {
                mios.add(current);
            }
        }

        if (filtroActual == 1) {
            anuncios.clear();


            adapter.resetStatusTask();
            adapter.notifyDataSetChanged();

            for (Anuncio filt : mios) anuncios.add(filt);

            adapter.resetStatusTask();
            adapter.notifyDataSetChanged();
        } else {
            anuncios.removeAll(mios);
            adapter.resetStatusTask();
            adapter.notifyDataSetChanged();
        }

    }

    public void getAnuncios(boolean start, int tipo) {
        filtroActual = tipo;

        System.out.println("entro a anuncios con " + filtroActual);
        //si es peticion inicial, no swipe, fuerzo el relojito
        if (start) {
            swipeContainer.post(new Runnable() {
                @Override
                public void run() {
                    swipeContainer.setRefreshing(true);
                }
            });
        }

		/*https://futurestud.io/tutorials/retrofit-getting-started-and-android-client
        http://www.jsonschema2pojo.org/
		https://guides.codepath.com/android/Consuming-APIs-with-Retrofit
		*/
        apiRequest = RestClient.getApiService().obtenerAnuncios();


        isRequestRunning = true;
        apiRequest.enqueue(new Callback<List<Anuncio>>() {

            @Override
            public void onResponse(Call<List<Anuncio>> call, Response<List<Anuncio>> response) {

                System.out.println("Llego al onResponse");
                swipeContainer.setRefreshing(false);
                needToUpdate = false;
                anuncios.clear();
                System.out.println(response);
                for (Anuncio uno : response.body()) {
                    anuncios.add(uno);
                    Log.d(TAG, uno.get_id());
                }

                //des-selecciono todos
                adapter.resetStatusTask();
//				disableEnableButtonsUI(false);
                adapter.notifyDataSetChanged();
                isRequestRunning = false;

                if (session.isLoggedIn()) {
                    filtrarMisAnuncios();
                }
            }

            @Override
            public void onFailure(Call<List<Anuncio>> call, Throwable t) {

                if (apiRequest.isCanceled()) {
                    Log.e(TAG, "Peticion cancelada bajo demanda");
                } else {
                    Log.e(TAG, t.getMessage());
                    Log.e(TAG, t.getCause().getMessage());
                }
                isRequestRunning = false;
            }
        });


    }


}