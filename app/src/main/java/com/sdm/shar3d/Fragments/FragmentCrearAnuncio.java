package com.sdm.shar3d.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.REST.Anuncio;
import com.sdm.shar3d.REST.AnuncioResponse;
import com.sdm.shar3d.REST.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class FragmentCrearAnuncio extends Fragment {


    private String TAG = "FragmentCrearAnuncio";
    private View topView;
    private SharedPreferencesHelper session;
    EditText contact;
    EditText description;
    EditText zona;
    EditText price;
    Button crear;
    Anuncio anuncio;
    ProgressDialog pd;
    private FragmentActivity parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_new_anuncio, container, false);
        session = new SharedPreferencesHelper(getActivity());
        parent = getActivity();


        //Muestro toolbar cuando entro en un Fragment
        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);

        session = new SharedPreferencesHelper(getActivity());

        crear = (Button) topView.findViewById(R.id.creaAnuncio);
        contact = (EditText) topView.findViewById(R.id.contactView);
        description = (EditText) topView.findViewById(R.id.descView);
        price = (EditText) topView.findViewById(R.id.precioView);
        zona = (EditText) topView.findViewById(R.id.locaView);


        crear.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View view) {
                    Call<AnuncioResponse> apiRequest;
                    String contacto = contact.getText().toString();
                    String descripcion = description.getText().toString();
                    String precio = price.getText().toString();
                    String localizacion = zona.getText().toString();
                    double latitude = session.getLatitude();
                    double longitude = session.getLongitude();

                    pd = new ProgressDialog(parent);
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    pd.setMessage("Espere por favor");
                    pd.show();

                    anuncio = new Anuncio();
                    anuncio.setContacto(contacto);
                    anuncio.setDescripcion(descripcion);
                    anuncio.setPrecio(precio);
                    anuncio.setUsername(session.getUser());
                    anuncio.setPrintername(session.getPrinterName());
                    anuncio.setDescPrinter(session.getPrinterDescription());
                    anuncio.setImgPrinter(session.getPrinterImage());
                    anuncio.setZona(localizacion);
                    anuncio.setLatitude(latitude);
                    anuncio.setLongitude(longitude);

                    apiRequest = RestClient.getApiService().publicaAnuncio(anuncio);
                    apiRequest.enqueue(new Callback<AnuncioResponse>() {

                    @Override
                    public void onResponse(Call<AnuncioResponse> call, Response<AnuncioResponse> response) {
                        System.out.println("publicado anuncio");

                        if(pd.isShowing())
                            pd.dismiss();
                        FragmentAnuncios nextFrag = new FragmentAnuncios();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, nextFrag)
                                .commit();

                    }

                    @Override
                    public void onFailure(Call<AnuncioResponse> call, Throwable t) {

                        System.out.println("Error al publicar");

                        Log.e(TAG, t.getMessage());
                        Log.e(TAG, t.getCause().getMessage());
                        if(pd.isShowing())
                            pd.dismiss();

                    }
                });

            }
        });

        return topView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPause() {

        super.onPause();
    }


    //   Método para la imagen de usuario
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == RESULT_OK) {
        }
    }


}