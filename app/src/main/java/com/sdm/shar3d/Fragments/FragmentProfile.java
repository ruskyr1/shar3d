package com.sdm.shar3d.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Activities.MainActivity;
import com.sdm.shar3d.Helpers.CircleTransform;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.REST.RestClient;
import com.sdm.shar3d.REST.TokenResponse;
import com.sdm.shar3d.pojo.User;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import javax.crypto.SecretKey;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class FragmentProfile extends Fragment {


    private SharedPreferencesHelper session;
    private String TAG = "FragmentProfile";
    private ProgressDialog mDialog;
    private View topView;
    private SecretKey key;
    private long startTime;
    private ImageView contactImageImgView;
    private CheckBox checkNotifications;
    ImageView drawerProfileImage;
    NavigationView navView;
    View navHeader;
    Uri imageUri;
    ProgressDialog pd;
    Button mapButton;
    FrameLayout frameMap;
    private FragmentActivity parent;

    String image64;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_profile, container, false);

        parent = getActivity();
        contactImageImgView = (ImageView) topView.findViewById(R.id.imagenPerfil);
        checkNotifications = (CheckBox) topView.findViewById(R.id.checkBoxNotif);
        navView = (NavigationView) parent.findViewById(R.id.nav_view);
        navHeader = navView.getHeaderView(0);
        drawerProfileImage = (ImageView) navHeader.findViewById(R.id.imageProfile);
        mapButton = (Button) topView.findViewById(R.id.mapButton);
        frameMap = (FrameLayout) topView.findViewById(R.id.frameMap);
        session = new SharedPreferencesHelper(parent);


        //Muestro toolbar cuando entro en un Fragment
        AppBarLayout appBarLayout = (AppBarLayout) parent.findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);

        if (session.isLoggedIn()) {
            topView.findViewById(R.id.nombreTxtProf).setVisibility(View.VISIBLE);
            ((TextView) topView.findViewById(R.id.nombreTxtProf)).setText(session.getUser());
            ((TextView) topView.findViewById(R.id.emailTxtProf)).setText(session.getEmail());
            image64 = session.getImageProfile();

            Log.v(TAG, "+Comparativa gettoken: " + session.getKeyNotifications());
            if (session.getKeyNotifications().equals("false")) checkNotifications.setChecked(false);
            else checkNotifications.setChecked(true);

        } else {
        }

        if(getArguments() != null){
            if(getArguments().getString("user") != null) {
                topView.findViewById(R.id.buttonGuardar).setVisibility(View.GONE);
                topView.findViewById(R.id.logout).setVisibility(View.GONE);
                mapButton.setVisibility(View.GONE);
                GsonBuilder gb = new GsonBuilder();
                gb.serializeNulls();
                Gson gson = gb.create();
                String profileUserJson = getArguments().getString("user");
                User profileUser = gson.fromJson(profileUserJson, User.class);
                ((TextView) topView.findViewById(R.id.nombreTxtProf)).setText(profileUser.getUsername());
                Log.d(TAG, profileUser.getUsername());
                ((TextView) topView.findViewById(R.id.emailTxtProf)).setText(profileUser.getEmail());
                image64 = profileUser.getImage();
            }
        }else{
            topView.findViewById(R.id.flechaEdit1).setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            topView.findViewById(R.id.flechaEdit1).setVisibility(View.GONE);
                        }
                    }
            );


            topView.findViewById(R.id.logout).setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            session.logout();

                            FragmentAnuncios nextFrag = new FragmentAnuncios();
                            parent.getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, nextFrag)
                                    .commit();
                            ((MainActivity) parent).loadNavHeader();

                        }
                    }
            );

            topView.findViewById(R.id.buttonGuardar).setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View view) {

                            //Coge la imágen actual del ImageView, la transforma a base64 y la mete en session.
                            String base64image;
                            try {
                                Bitmap bitmap = ((GlideBitmapDrawable) contactImageImgView.getDrawable().getCurrent()).getBitmap();
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                base64image = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                session.setImageProfile(base64image);
                                ((MainActivity) parent).loadNavHeader();
                                Utils.changeProfileImage(parent, base64image, session.getUser());
                            } catch (ClassCastException e) {
                                e.printStackTrace();
                            }

                            FragmentAnuncios nextFrag = new FragmentAnuncios();
                            parent.getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, nextFrag)
                                    .commit();
                        }
                    }
            );

            //Declaraciones para la imágen del perfil de usuario
            contactImageImgView = (ImageView) topView.findViewById(R.id.imagenPerfil);
            contactImageImgView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    imageUri = Utils.createFileUri(parent);
                    Intent chooser = Utils.getMediaChooser(imageUri);
                    startActivityForResult(chooser, 1);


                }

            });

            mapButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Fragment nextFrag = new FragmentSetLocation();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, nextFrag)
                            .commit();
                }
            });
        }
        try {
            Glide.with(this).load(Base64.decode(image64, Base64.DEFAULT))
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(parent))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(contactImageImgView);
        }catch (Exception e){
            e.printStackTrace();
        }




        topView.findViewById(R.id.checkBoxNotif).setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View v) {

                                                                            String FireBasetoken = FirebaseInstanceId.getInstance().getToken();


                                                                            if (checkNotifications.isChecked()) {
                                                                                Log.v(TAG, "Activo True");


                                                                                HashMap<String, String> token = new HashMap<String, String>();

                                                                                token.put("token", FireBasetoken);

                                                                                Call<TokenResponse> apiRequest = RestClient.getApiService().registerNotificationToken(token);

                                                                                apiRequest.enqueue(new Callback<TokenResponse>() {
                                                                                    @Override
                                                                                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                                                                                        Log.v(TAG, "+Response.message ACTIVAR: " + response.message());
                                                                                        session.setKeyNotifications(response.body().getInsertedToken().getId());
                                                                                    }

                                                                                    @Override
                                                                                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                                                                                        session.setKeyNotifications("false");
                                                                                        Log.e(TAG, "+Error ACTIVAR: " + t.getMessage());
                                                                                        Log.e(TAG, "+Error ACTIVAR: " + t.getCause().getMessage());

                                                                                    }
                                                                                });
                                                                            } else {
                                                                                Log.v(TAG, "Activo false");

                                                                                Call<ResponseBody> apiRequest = RestClient.getApiService().deleteNotificationToken(session.getKeyNotifications());

                                                                                apiRequest.enqueue(new Callback<ResponseBody>() {
                                                                                    @Override
                                                                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                                                        Log.v(TAG, "+Response.message DESACTIVAR: " + response.message());
                                                                                        session.setKeyNotifications("false");
                                                                                    }

                                                                                    @Override
                                                                                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                                                                                        Log.e(TAG, "+ErrorTokenDeleted: " + t.getMessage());
                                                                                        Log.e(TAG, "+ErrorTokenDeleted: " + t.getCause().getMessage());

                                                                                    }
                                                                                });

                                                                            }
                                                                        }
                                                                    }
        );




        return topView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPause() {

        super.onPause();
    }


    //   Método para la imagen de usuario
    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Log.d("FragmentProfileOnRespon", String.valueOf(reqCode));
        if (resCode == RESULT_OK) {
            if (reqCode == 1) {


                Log.d(TAG, "Imagen cambiada!!!!!");
                contactImageImgView.getDrawable();

                if (data != null && null != data.getDataString()) {
                    Log.d(TAG, "GALERIA");
                    String uri = data.getDataString();

                    Glide.with(this).load(uri)
                            .crossFade()
                            //.thumbnail(0.5f)
                            .bitmapTransform(new CircleTransform(parent))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(contactImageImgView);
                } else {
                    Log.d(TAG, "CAMARA");

                    Utils.compressBitmap(imageUri, 10, parent);

                    Glide.with(this).load(imageUri)
                            .crossFade()
                            //.thumbnail(0.5f)
                            .bitmapTransform(new CircleTransform(parent))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(contactImageImgView);
                }

            } else if (reqCode == 2) {
                Log.d("RETURN DEL MAPS", "Ubicación seleccionada.");
                Place place = PlacePicker.getPlace(parent, data);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(parent, toastMsg, Toast.LENGTH_LONG).show();
                Log.d("RETURN DEL MAPS", "Ubicación seleccionada.");

            } else if (reqCode == 3) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    Intent intent = builder.build(parent);
                    startActivityForResult(intent, 2);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                Log.d("START PLACEPICKER", "Abriendo mapas...");

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Perfil");
        Bundle args = new Bundle();
        if(getArguments() == null) {
            args.putDouble("latitude", session.getLatitude());
            args.putDouble("longitude", session.getLongitude());
            args.putDouble("radius", session.getRadius());
        }else{
            if(getArguments().getString("user") != null) {
                GsonBuilder gb = new GsonBuilder();
                gb.serializeNulls();
                Gson gson = gb.create();
                String profileUserJson = getArguments().getString("user");
                User profileUser = gson.fromJson(profileUserJson, User.class);
                args.putDouble("latitude", profileUser.getLatitude());
                args.putDouble("longitude", profileUser.getLongitude());
                args.putDouble("radius", profileUser.getRadius());
            }
        }
        FragmentMap fragmentMap = new FragmentMap();
        fragmentMap.setArguments(args);

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameMap, fragmentMap);
        fragmentTransaction.commit();
    }
}