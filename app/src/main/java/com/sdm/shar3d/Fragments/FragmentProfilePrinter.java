package com.sdm.shar3d.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;
import com.sdm.shar3d.pojo.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfilePrinter extends Fragment {

    private View topView;
    private FrameLayout frameLayout;
    Fragment nextFrag;
    User profileUser;
    String profileUserJson;
    ProgressDialog pd;
    public final static String TAG = "FragmentProfilePrinter";

    public FragmentProfilePrinter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        topView = inflater.inflate(R.layout.fragment_profile_printer, container, false);
        frameLayout = (FrameLayout) topView.findViewById(R.id.frameLayout);

        //menu de la action bar
        setHasOptionsMenu(true);
        //Muestro toolbar cuando entro en un Fragment
        AppBarLayout appBarLayout = (AppBarLayout)getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);

        //TabLayout
        final TabLayout tabLayout = (TabLayout) topView.findViewById(R.id.profileTabs);
        tabLayout.addTab(tabLayout.newTab().setText("Info."));
        tabLayout.addTab(tabLayout.newTab().setText("Perfil"));
        //tabLayout.addTab(tabLayout.newTab().setText("Galería"));


        //SI SE ACCEDE DESDE EL BOTÓN "VER PERFIL" DE UN ANUNCIO:
        if(getArguments()!= null){
            String username = getArguments().getString("username");
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Cargando perfil...");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();

            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            final Gson gson = gb.create();
            final JsonArrayRequest req = new JsonArrayRequest(
                    Request.Method.GET, Utils.getLoginURL(username), null, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {


                    //LLAMADA VIEJA
                    if (response.length() != 0) {
                        JSONObject user;
                        try {
                            user = response.getJSONObject(0);
                            profileUser = gson.fromJson(user.toString(), User.class);
                            profileUserJson = user.toString();
                            Log.d(TAG, profileUserJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else
                        Snackbar.make(topView, "El usuario seleccionado no existe.", Snackbar.LENGTH_LONG).show();

                    if(pd.isShowing()) {
                        pd.dismiss();
                    }

                    tabLayout.getTabAt(0).select();
                    tabLayout.getTabAt(1).select();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse response = error.networkResponse;
                    if(response != null){
                        Log.e(TAG, new String(response.data));
                        Log.e(TAG, error.getCause().getMessage());
                    }else{
                        Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                    }

                    if(pd.isShowing())
                        pd.dismiss();

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frameLayout, new FragmentAnuncios())
                            .commit();
                }
            });
            RequestService.getInstance(getActivity()).addToQueue(req);
        }




        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Bundle args;
                switch (tab.getPosition()){

                    case 0:


                        nextFrag = new FragmentProfilePrinterInfo();
                        if(getArguments()!= null) {
                            args = new Bundle();
                            args.putString("user", profileUserJson);
                            nextFrag.setArguments(args);
                        }
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frameLayout, nextFrag)
                                .commit();
                        break;
                    case 1:
                        nextFrag = new FragmentProfile();
                        if(getArguments()!= null) {
                            args = new Bundle();
                            args.putString("user", profileUserJson);
                            nextFrag.setArguments(args);
                        }
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frameLayout, nextFrag)
                                .commit();
                        break;
                    case 2:
                        /*nextFrag = new FragmentSetLocation();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.frameLayout, nextFrag)
                                .commit();*/
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //Nothing
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //Nothing
            }
        });

        tabLayout.getTabAt(1).select();

        topView.setFocusableInTouchMode(true);
        topView.requestFocus();
        topView.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new FragmentAnuncios())
                            .commit();
                }
                return true;
            }
        } );


        return topView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }


}
