package com.sdm.shar3d.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sdm.shar3d.Helpers.CircleTransform;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;

import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;


public class FragmentAnuncioAmp extends Fragment {

    private String TAG = "FragmentAnuncioAmp";
    private View topView;
    private SharedPreferencesHelper session;
    private RequestService service;
    private ProgressDialog pd;

    Button verPerfil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.cardview_anuncio_expandido, container, false);
        session = new SharedPreferencesHelper(getActivity());

        //Muestro toolbar cuando entro en un Fragment
        service = RequestService.getInstance(getActivity());

        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);

        session = new SharedPreferencesHelper(getActivity());
        final Bundle bundle = this.getArguments();

        verPerfil = (Button) topView.findViewById(R.id.openProfile);

        ((TextView) topView.findViewById(R.id.printernameView)).setText(bundle.getString("printer"));
        ((TextView) topView.findViewById(R.id.usernameView)).setText(bundle.getString("nombre"));
        ((TextView) topView.findViewById(R.id.zonaView)).setText(bundle.getString("zona"));
        ((TextView) topView.findViewById(R.id.descView)).setText(bundle.getString("descripcion"));
        ((TextView) topView.findViewById(R.id.contactView)).setText(bundle.getString("contactView"));


        //Si es un usuario logueado viendo sus propios Anuncios:
        if (session.isLoggedIn() && bundle.getString("nombre").equals(session.getUser())) {
            verPerfil.setText(R.string.bBorrarAnuncio);
            verPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pd = new ProgressDialog(getActivity());
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    pd.setMessage(getString(R.string.BorrandoMensajePD));
                    pd.show();

                    try {
                        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, Utils.getAdDeleteURL(getArguments().getString("id")), null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG, response.toString());

                                Fragment nextFrag = new FragmentAnuncios();

                                if (pd.isShowing())
                                    pd.dismiss();

                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, nextFrag)
                                        .commit();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    String json = new String(response.data);
                                    Log.d(TAG, json);
                                    Snackbar.make(topView, "Ha habido un error. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                                } else {
                                    if(error != null)
                                    Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                                }
                                if (pd.isShowing())
                                    pd.dismiss();
                            }
                        });
                        service.addToQueue(req);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (pd.isShowing())
                            pd.dismiss();
                        Snackbar.make(topView, "Ha habido un error. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                    }
                }
            });
        }else{
            verPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle args = new Bundle();
                    args.putString("username", bundle.getString("nombre"));
                    Fragment nextFrag = new FragmentProfilePrinter();
                    nextFrag.setArguments(args);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, nextFrag)
                            .commit();

                }
            });
        }

        try {
            Glide.with(this).load(Base64.decode(bundle.getString("printerImg"), Base64.DEFAULT))
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into((ImageView) (topView.findViewById(R.id.imagenImpresora)));
        } catch (Exception e) {
            System.out.println(e);
        }


        topView.findViewById(R.id.close).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        android.support.v4.app.FragmentManager fm = getFragmentManager();
                        Fragment nextFrag = new FragmentAnuncios();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, nextFrag).commit();
                        /*fm.popBackStack();
                        fm.executePendingTransactions();*/
                    }
                }
        );

        return topView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPause() {

        super.onPause();
    }


    //   Método para la imagen de usuario
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == RESULT_OK) {
/*            if (reqCode == 1)
                contactImageImgView.setImageURI(data.getData());*/
        }
    }


}