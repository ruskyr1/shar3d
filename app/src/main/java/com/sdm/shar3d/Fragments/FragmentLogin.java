package com.sdm.shar3d.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.sdm.shar3d.Activities.MainActivity;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FragmentLogin extends Fragment {


    private View topView;
    private FragmentActivity parent;
    private String TAG = "FragmentLogin";
    private boolean tokenFounded = false;

    RequestService service;
    ProgressDialog pd;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_login, container, false);

        parent = getActivity();

        service = RequestService.getInstance(parent);

        if (parent.getIntent().getExtras() != null) {
            for (String key : parent.getIntent().getExtras().keySet()) {
                Object value = parent.getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }


        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);


        //reinicio datos
        if (savedInstanceState != null) {

        } else {

        }


        //UI listeners
        Button signup = (Button) topView.findViewById(R.id.regis);
        signup.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Log.d(TAG, "A registrarseeee!!");
                        FragmentSignup nextFrag = new FragmentSignup();


                        parent.getSupportFragmentManager().beginTransaction()
                                .add(nextFrag, TAG)
                                .replace(R.id.content_frame, nextFrag)
                                .commit();
                    }
                }
        );

        Button comenzar = (Button) topView.findViewById(R.id.login);
        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText usuario = (EditText) topView.findViewById(R.id.user);
                EditText pass = (EditText) topView.findViewById(R.id.pass);

				/*if(!Utils.checkEmaill(usuario.getText().toString())){
                    Toast.makeText(parent, "Introduce una direccion de correo valida", Toast.LENGTH_SHORT).show();

				}
				else */
                if (pass.getText().toString().length() == 0 || pass.getText().toString().isEmpty()) {
                    Toast.makeText(parent, "Introduce una contraseña valida", Toast.LENGTH_SHORT).show();
                } else {
                    pd = new ProgressDialog(parent);
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    pd.setMessage("Comprobando usuario");
                    pd.show();

                    HashMap<String, String> loginInfo = new HashMap<String, String>();

                    loginInfo.put("email", usuario.getText().toString());
                    loginInfo.put("password", pass.getText().toString());


                    String username = usuario.getText().toString().trim();
                    final String password = pass.getText().toString().trim();


                    JsonArrayRequest req = new JsonArrayRequest(
                            Request.Method.GET, Utils.getLoginURL(username), null, new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {


                            //LLAMADA VIEJA
                            if (response.length() != 0) {
                                JSONObject user;
                                try {
                                    user = response.getJSONObject(0);

                                    if (password.equals(user.get("password"))) {

                                        Log.v(TAG, "//Login-> CORRECTO");
                                        final SharedPreferencesHelper session = new SharedPreferencesHelper(parent);
                                        session.createLoginSession(user.getString("email"));
                                        session.setUser(user.getString("username"));
                                        session.setImageProfile(user.getString("image"));
                                        boolean isPrinter = !user.get("printer").toString().equals("null");
                                        Log.d(TAG, String.valueOf(isPrinter));
                                        session.setIsPrinter(isPrinter);

                                        if(isPrinter) {
                                            session.setPrinterName(user.getJSONObject("printer").getString("name"));
                                            session.setPrinterDescription(user.getJSONObject("printer").getString("description"));
                                            session.setPrinterImage(user.getJSONObject("printer").getString("image"));
                                            session.setIsDesigner(user.getBoolean("designer"));
                                        }
                                        try {
                                            session.setLatitude(user.getDouble("latitude"));
                                            session.setLongitude(user.getDouble("longitude"));
                                            session.setRadius(user.getDouble("radius"));
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                        if (!tokenFounded) session.setKeyNotifications("false");


                                        FragmentAnuncios nextFrag = new FragmentAnuncios();
                                        getActivity().getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.content_frame, nextFrag)
                                                .commit();

                                        ((MainActivity) getActivity()).loadNavHeader();

                                    } else
                                        Snackbar.make(topView, "Contraseña incorrecta, revise la información propocionada", Snackbar.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else
                                Snackbar.make(topView, "El usuario no existe en nuestra base de datos, revise la información propocionada", Snackbar.LENGTH_LONG).show();

                            if(pd.isShowing())
                                pd.dismiss();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkResponse response = error.networkResponse;
                            if(response != null){
                                Log.e(TAG, new String(response.data));
                                Log.e(TAG, error.getCause().getMessage());
                            }else{
                                Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                            }

                            if(pd.isShowing())
                                pd.dismiss();
                        }
                    });
                    service.addToQueue(req);


                }


            }
        });


        //inicio-reinicio
        if (savedInstanceState == null) { //inicio
        } else {
            //reinicio
        }


        return topView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        //recupero los datos del estado guardado
        super.onSaveInstanceState(outState);
    }


}