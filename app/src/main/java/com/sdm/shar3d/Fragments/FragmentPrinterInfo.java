package com.sdm.shar3d.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;
import com.sdm.shar3d.pojo.Printer;
import com.sdm.shar3d.pojo.User;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;


public class FragmentPrinterInfo extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final int MEDIA_CODE = 1;
    GsonBuilder gb;
    Gson gson;
    User user;
    private View topView;
    private FragmentActivity parent;
    private String TAG = "FragmentPrinterInfo";
    RequestService service;
    Uri imageUri;
    ProgressDialog pd;


    Button comenzar;
    EditText printerName;
    EditText printerDescription;
    CheckBox isDesigner;
    ImageView printerImage;

    public FragmentPrinterInfo() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gb = new GsonBuilder().serializeNulls();
        gson = gb.create();
        if (getArguments() != null) {
            user = gson.fromJson(getArguments().getString("user"), User.class);
            Log.d(TAG, user.toString());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        topView = inflater.inflate(R.layout.fragment_printer_info, container, false);

        parent = getActivity();

        service = RequestService.getInstance(parent);

        if (parent.getIntent().getExtras() != null) {
            for (String key : parent.getIntent().getExtras().keySet()) {
                Object value = parent.getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }


        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);


        comenzar = (Button) topView.findViewById(R.id.buttonNext);
        printerName = (EditText) topView.findViewById(R.id.printerName);
        printerDescription = (EditText) topView.findViewById(R.id.printerDescription);
        isDesigner = (CheckBox) topView.findViewById(R.id.checkBoxIsDesigner);
        printerImage = (ImageView) topView.findViewById(R.id.imagePrinter);

        //OnClickListeners
        printerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageUri = Utils.createFileUri(getActivity());
                Intent chooser = Utils.getMediaChooser(imageUri);
                startActivityForResult(chooser, MEDIA_CODE);
            }
        });

        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = printerName.getText().toString();
                String description = printerDescription.getText().toString();

                if (name.length() != 0 && !name.isEmpty()) {
                    if (description.length() != 0 && !description.isEmpty()) {

                        pd = new ProgressDialog(parent);
                        pd.setCancelable(false);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setMessage("Espera por favor");
                        pd.show();

                        Printer printer = new Printer();
                        printer.setName(name);
                        printer.setDescription(description);
                        //Si la imagen asignada al view no es nula, la recupera y la convierte en BASE64.
                        if (printerImage.getDrawable() != null){
                            Bitmap bitmap = null;
                            String bitmap64 = null;
                            try {
                                bitmap = ((GlideBitmapDrawable) printerImage.getDrawable().getCurrent()).getBitmap();
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                byte[] byteArray = byteArrayOutputStream.toByteArray();
                                bitmap64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                            }catch (ClassCastException e){
                                e.printStackTrace();
                            }
                            printer.setImage(bitmap64);
                        }
                        if(isDesigner.isChecked()){
                            user.setDesigner(true);
                        }else
                            user.setDesigner(false);

                        user.setPrinter(printer);
                        final String jsonUser = gson.toJson(user);
                        try {
                            JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, Utils.getUpdateProfileURL(user.getUsername()), new JSONObject(jsonUser), new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d(TAG, response.toString());

                                    Bundle args = new Bundle();
                                    args.putString("user", jsonUser);
                                    if(pd.isShowing())
                                        pd.dismiss();

                                    Fragment nextFrag = new FragmentProfilePicture();
                                    //nextFrag = new FragmentProfilePicture();
                                    nextFrag.setArguments(args);
                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, nextFrag)
                                            .commit();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    NetworkResponse response = error.networkResponse;
                                    if (response != null) {
                                        String json = new String(response.data);
                                        Log.d(TAG, json);
                                    } else {
                                        Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                                    }
                                    if(pd.isShowing())
                                        pd.dismiss();
                                }
                            });
                            RequestService.getInstance(getContext()).addToQueue(req);
                        } catch (Exception a) {
                            a.printStackTrace();
                        }

                    } else
                        Snackbar.make(topView, "Indica una descripción para la impresora.", Snackbar.LENGTH_LONG).show();
                } else
                    Snackbar.make(topView, "Indica el nombre de la impresora.", Snackbar.LENGTH_LONG).show();
            }
        });


        return topView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEDIA_CODE && resultCode == RESULT_OK) {
            Log.d(TAG, "Imagen cambiada!!!!!");
            if (null != data && data.getDataString() != null) {
                Log.d(TAG, "GALERIA");
                String uri = data.getDataString();
                //session.setImageProfile(uri);
                //contactImageImgView.setImageURI(data.getData());
                Glide.with(this).load(uri)
                        .crossFade()
                        //.thumbnail(1f)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(printerImage);
            } else {
                Log.d(TAG, "CAMARA");
                Utils.compressBitmap(imageUri, 10, getActivity());
                //Uri uri = Utils.bitmapToFile(imageBitmap, getActivity());
                //session.setImageProfile(uri.toString());
                Glide.with(this).load(imageUri)
                        .crossFade()
                        //.thumbnail(1f)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(printerImage);
            }
        }
    }
}

