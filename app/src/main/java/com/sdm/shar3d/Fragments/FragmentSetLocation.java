package com.sdm.shar3d.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetLocation extends Fragment {

    public final static String TAG = "FragmentSetLocation";
    Activity parent;
    View topView;
    RequestService service;
    Button mapButton;
    SeekBar sliderKms;
    TextView kms;
    CheckBox noKms;
    Button start;
    FrameLayout frameMap;

    ProgressDialog pd;

    SharedPreferencesHelper session;

    String username;

    double latitude;
    double longitude;
    double radius;

    public static int PLACEPICKER_INTENT = 2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        topView = inflater.inflate(R.layout.fragment_set_location, container, false);
        parent = getActivity();
        session = new SharedPreferencesHelper(parent);

        service = RequestService.getInstance(parent);


        mapButton = (Button) topView.findViewById(R.id.buttonMap);
        sliderKms = (SeekBar) topView.findViewById(R.id.sliderKms);
        kms = (TextView) topView.findViewById(R.id.KMTextView);
        noKms = (CheckBox) topView.findViewById(R.id.checkBoxNoRadius);
        start = (Button) topView.findViewById(R.id.buttonStart);
        frameMap = (FrameLayout) topView.findViewById(R.id.frameMap);

        if (getArguments() != null) {
            Log.d(TAG, "De arguments!");
            username = getArguments().getString("user");
            Log.d(TAG, username);
        } else {
            Log.d(TAG, "De session!");
            username = session.getUser();
            longitude = session.getLongitude();
            latitude = session.getLatitude();
            radius = session.getRadius();
        }

        updateMap();
        //Listenerssssssssss

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ContextCompat.checkSelfPermission(parent,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(parent,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
                    } else {
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        Intent intent = builder.build(parent);
                        startActivityForResult(intent, PLACEPICKER_INTENT);
                        Log.d("START PLACEPICKER", "Abriendo mapas...");

                    }
                } catch (GooglePlayServicesRepairableException e) {
                    GooglePlayServicesUtil
                            .getErrorDialog(e.getConnectionStatusCode(), getActivity(), 0);
                } catch (GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(getActivity(), "Google Play Services is not available.",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        noKms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sliderKms.setEnabled(false);
                    updateKms(0);
                } else {
                    sliderKms.setEnabled(true);
                    updateKms(sliderKms.getProgress());
                }
            }
        });

        sliderKms.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateKms(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //nanai
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //nanai
            }
        });

        //Ajusto el estado inicial del SEEKBAR después de asignarle el listener:
        if(radius == 0.0)
            sliderKms.setProgress(500);
        else sliderKms.setProgress(Double.valueOf(radius).intValue());

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Comprobar que está ttodo en orden, enviarlo al servidor e ir al fragment ofertas
                 */
                //Log.d(TAG, username);
                if (latitude != 0.0 && longitude != 0.0){
                    //Utils.changeUserLocation(parent, latitude, longitude, radius, username);
                    try {
                        pd = new ProgressDialog(parent);
                        pd.setMessage("Espera, por favor.");
                        pd.setCanceledOnTouchOutside(false);
                        pd.setCancelable(false);
                        pd.show();
                        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, Utils.getUpdateProfileURL(username), new JSONObject(Utils.getUpdateLocationTemplate(latitude, longitude, radius)), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                if(pd.isShowing())
                                    pd.dismiss();
                                Toast.makeText(parent, "Ubicación modificada con éxito.", Toast.LENGTH_SHORT).show();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(pd.isShowing())
                                    pd.dismiss();
                                NetworkResponse nr = error.networkResponse;
                                if(nr!= null){
                                    Log.d("UTILS", nr.toString());
                                }else{
                                    Toast.makeText(parent, "Ha habido un error con la red. Inténtalo más tarde.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        service.addToQueue(req);
                    } catch (JSONException e) {
                        if(pd.isShowing())
                            pd.dismiss();
                        e.printStackTrace();
                    }



                    Fragment nextFrag;
                    if(getArguments() != null)
                        nextFrag = new FragmentAnuncios();
                    else {

                        session.setLongitude(longitude);
                        session.setLatitude(latitude);
                        session.setRadius(radius);
                        if (session.getIsPrinter())
                            nextFrag = new FragmentProfilePrinter();
                        else
                            nextFrag = new FragmentProfile();

                    }
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, nextFrag)
                            .commit();

                }else
                    Toast.makeText(parent, "Por favor, selecciona una ubicación", Toast.LENGTH_SHORT).show();

            }
        });


        return topView;
    }

    private void updateKms(int distance) {
        kms.setText(String.format("%s Metros", String.valueOf(distance)));
        radius = distance;
        updateMap();
    }

    private void updateMap() {
        frameMap.setVisibility(View.VISIBLE);
        Bundle args = new Bundle();
        args.putDouble("latitude", latitude);
        args.putDouble("longitude", longitude);
        args.putDouble("radius", radius);
        FragmentMap fragmentMap = new FragmentMap();
        fragmentMap.setArguments(args);

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameMap, fragmentMap);
        fragmentTransaction.addToBackStack("B");
        fragmentTransaction.commit();


        Log.d("Prueba", String.valueOf(this.latitude));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACEPICKER_INTENT) {

                Place place = PlacePicker.getPlace(parent, data);
                LatLng latLng = place.getLatLng();
                this.latitude = latLng.latitude;
                this.longitude = latLng.longitude;


                //updateMap();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateMap();
    }
}
