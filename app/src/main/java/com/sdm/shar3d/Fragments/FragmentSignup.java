package com.sdm.shar3d.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.R;
import com.sdm.shar3d.Services.RequestService;
import com.sdm.shar3d.pojo.User;

import org.json.JSONException;
import org.json.JSONObject;


public class FragmentSignup extends Fragment {

    private View topView;
    private FragmentActivity parent;
    private String TAG = "FragmentSignUp";
    private CheckBox isPrinter;

    ProgressDialog pd;

    RequestService service;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        topView = inflater.inflate(R.layout.fragment_fragment_signup, container, false);

        isPrinter = (CheckBox) topView.findViewById(R.id.checkBoxHaveAPrinter);
        parent = getActivity();

        service = RequestService.getInstance(parent);

        if (parent.getIntent().getExtras() != null) {
            for (String key : parent.getIntent().getExtras().keySet()) {
                Object value = parent.getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }


        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true, true);


        //OnClickListeners
        Button comenzar = (Button) topView.findViewById(R.id.login);
        comenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                EditText usuario = (EditText) topView.findViewById(R.id.user);
                EditText pass = (EditText) topView.findViewById(R.id.pass);
                EditText mail = (EditText) topView.findViewById(R.id.mail);
                String username = usuario.getText().toString().trim();
                String password = pass.getText().toString().trim();
                String email = mail.getText().toString().trim();


                if (!Utils.checkEmaill(email)) {
                    Toast.makeText(parent, "Introduce una direccion de correo valida", Toast.LENGTH_SHORT).show();
                } else if (password.length() == 0 || password.isEmpty()) {
                    Toast.makeText(parent, "Introduce una contraseña valida", Toast.LENGTH_SHORT).show();
                } else if (username.isEmpty() || username.length() == 0) {
                    Toast.makeText(parent, "Introduce un usuario valido", Toast.LENGTH_SHORT).show();
                } else {

                    //El ProgressDialog isi pisi
                    pd = new ProgressDialog(parent);
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    pd.setMessage("Espera por favor");
                    pd.show();

                    final User user = new User();
                    user.setUsername(username);
                    user.setPassword(password);
                    user.setEmail(email);
                    //HTTP Request:
                    GsonBuilder gb = new GsonBuilder();
                    gb.serializeNulls();
                    Gson gson = gb.create();
                    final String jsonUser = gson.toJson(user);
                    try {
                        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, Utils.getSignUpURL(), new JSONObject(jsonUser), new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d(TAG, response.toString());
                                Bundle args = new Bundle();
                                args.putString("user", jsonUser);
                                if(pd.isShowing())
                                    pd.dismiss();
                                //IR A LA SIGUIENTE PANTALLA DEPENDIENDO DEL CHECKBOX.
                                Fragment nextFrag;
                                if (isPrinter.isChecked())
                                    nextFrag = new FragmentPrinterInfo();
                                else
                                    nextFrag = new FragmentProfilePicture();
                                nextFrag.setArguments(args);
                                getActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, nextFrag)
                                        .commit();
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    String json = new String(response.data);
                                    String msg = "";
                                    try {
                                        msg = new JSONObject(json).getString("message");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Log.d(TAG, msg);
                                    if (msg != null) {
                                        msg = msg.substring(msg.indexOf("dup key"));

                                        if (msg.contains(user.getUsername())) {
                                            Snackbar.make(topView, "El nombre usuario ya existe.", Snackbar.LENGTH_LONG).show();
                                        } else if (msg.contains(user.getEmail())) {
                                            Snackbar.make(topView, "El email ya existe.", Snackbar.LENGTH_LONG).show();
                                        }
                                    }
                                } else {
                                    Snackbar.make(topView, "Ha habido un error con la red. Inténtalo más tarde.", Snackbar.LENGTH_LONG).show();
                                }
                                if(pd.isShowing())
                                    pd.dismiss();

                            }
                        });
                        RequestService.getInstance(getContext()).addToQueue(req);
                    } catch (Exception a) {
                        a.printStackTrace();
                    }


                }
            }


        });


        return topView;
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        //recupero los datos del estado guardado

        super.onSaveInstanceState(outState);
    }


}
