package com.sdm.shar3d.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;

public class FragmentMap extends Fragment {

    public static final String TAG = "FragmentMap";

    MapView mMapView;
    private GoogleMap googleMap;
    Activity parent;
    double latitude = 39.4666667;
    double longitude = -0.3666667;
    double radius = 750;
    String markerTitle = "Título de la ubicación";
    String markerDescription = "Descripción de la ubicación";
    SharedPreferencesHelper session;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        parent = getActivity();
        session = new SharedPreferencesHelper(parent);

        try {
            Bundle args = this.getArguments();
            if (args != null) {
                Log.d(TAG, "De arguments!");
                if (args.getDouble("latitude") != 0.0)
                    latitude = args.getDouble("latitude");
                if (args.getDouble("longitude") != 0.0)
                    longitude = args.getDouble("longitude");
                radius = args.getDouble("radius");
            }else{
                Log.d(TAG, "De session!");
                latitude = session.getLatitude();
                longitude = session.getLongitude();
                radius = session.getRadius();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(parent, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(parent, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng point = new LatLng(latitude, longitude);
                googleMap.addMarker(new MarkerOptions().position(point).title("Marker Title").snippet("Marker Description"));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(point).zoom(12).build();

                Circle circle = mMap.addCircle(new CircleOptions()
                        .center(point)
                        .radius(radius)
                        .strokeColor(Color.alpha(100))
                        .fillColor(Color.argb(50, 63, 81, 181))
                );
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}