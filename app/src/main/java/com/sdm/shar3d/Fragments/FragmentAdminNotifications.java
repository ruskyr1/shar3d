package com.sdm.shar3d.Fragments;



import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;


public class FragmentAdminNotifications extends Fragment{


	private View topView;
	private FragmentActivity parent;
	private String TAG= "FragmentAdminNot";
	private SharedPreferencesHelper sesion;



	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        topView = inflater.inflate(R.layout.fragment_admin_notifications, container, false);

		parent = getActivity();
		sesion = new SharedPreferencesHelper(getActivity());
		if (parent.getIntent().getExtras() != null) {
			for (String key : parent.getIntent().getExtras().keySet()) {
				Object value = parent.getIntent().getExtras().get(key);
				Log.d(TAG, "Key: " + key + " Value: " + value);
			}
		}

		AppBarLayout appBarLayout = (AppBarLayout)getActivity().findViewById(R.id.appBarLayout);
		appBarLayout.setExpanded(true,true);


		//reinicio datos
		if (savedInstanceState != null) {

		} else {

		}



		//UI listeners
		Button subscribeButton = (Button) topView.findViewById(R.id.subscribeButton);
		subscribeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// [START subscribe_topics]
				FirebaseMessaging.getInstance().subscribeToTopic("news");
				// [END subscribe_topics]

				// Log and toast
				String msg = getString(R.string.msg_subscribed);
				Log.d(TAG, msg);
				Toast.makeText(parent, msg, Toast.LENGTH_SHORT).show();
			}
		});

		Button logTokenButton = (Button) topView.findViewById(R.id.logTokenButton);
		logTokenButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Get token
				String token = FirebaseInstanceId.getInstance().getToken();

				// Log and toast
				String msg = getString(R.string.msg_token_fmt, token);
				Log.d(TAG, msg);
				Toast.makeText(parent, msg, Toast.LENGTH_SHORT).show();
			}
		});



		//inicio-reinicio
		if (savedInstanceState == null){ //inicio
			 }
		else{
		//reinicio
		}


		return topView;
    }

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		//recupero los datos del estado guardado
		super.onSaveInstanceState(outState);
	}



	//////////////////////// AUX METHODS


}