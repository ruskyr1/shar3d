package com.sdm.shar3d.Persistency;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;



public class SharedPreferencesHelper {
	// Shared Preferences
	SharedPreferences pref;

	// Editor for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Sharedpref file name
	private static final String PREF_NAME = "shar3dPref";

	// All Shared Preferences Keys
	private static final String IS_LOGIN = "islogin";


	public static final String KEY_EMAIL = "email";

	private static final String KEY_PIRACY = "mathafucka";

	public static final String KEY_UPDATE = "notasked";

	public static final String KEY_USERLOGIN = "USERLOGIN";

	public static final String KEY_IMAGEPROFILE = "imageprofile";

	public static final String KEY_MEDIAPATH = "mediapath";

	public static final String KEY_PASSWORD = "PASSWORD";

	public static final String KEY_SERVER = "SERVER";

	public static final String KEY_WORKING = "WORKING";

	public static final String KEY_PUBLIC = "Public";

	public static final String KEY_PRIVATE = "Private";

	public static final String KEY_GUSTOS = "gustos";

	public static final String KEY_NOTIFICATIONS = "notifications";

	public static final String KEY_PRINTERIMAGE = "printerImage";

	public static final String KEY_ISPRINTER = "isPrinter";

	public static final String KEY_LATITUDE = "latitude";

	public static final String KEY_LONGITUDE = "longitude";

	public static final String KEY_RADIUS = "radius";

	public static final String KEY_PRINTER_NAME = "printerInfo";

	public static final String KEY_PRINTER_DESCRIPTION = "printerDescription";

	public static final String KEY_IS_DESIGNER = "isDesigner";

	public static final String KEY_PRINTER_IMAGE = "printerImage";

	// Constructor
	public SharedPreferencesHelper(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public SharedPreferencesHelper(){
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	//session.createLoginSession(usuario,codusuario,nombreusuario,codcentro,nombrecentro, ubicacion,calidad,language);

	public void createLoginSession(String e){


		editor.putString(KEY_EMAIL, e);
		setLogin();

	}


	public String getUser(){ return  pref.getString(KEY_USERLOGIN, null); }

	public Boolean getIsPrinter(){return pref.getBoolean(KEY_ISPRINTER, false); }

	public String getEmail(){ return  pref.getString(KEY_EMAIL, null); }

	public String getImageProfile(){ return  pref.getString(KEY_IMAGEPROFILE, null); }

	public String getPrinterImage(){return pref.getString(KEY_PRINTERIMAGE, null); }

	public String getPassword(){ return  pref.getString(KEY_PASSWORD, null); }

	public String getPiracy(){ return  pref.getString(KEY_PIRACY, null); }

	public boolean isLoggedIn(){ return pref.getBoolean(IS_LOGIN, false); }

	public String getMediaPath(){ return  pref.getString(KEY_MEDIAPATH, null); }

	public String getServer(){ return  pref.getString(KEY_SERVER, null); }

	public String getWorking(){ return  pref.getString(KEY_WORKING, null); }

	public String getPublicKey(){ return  pref.getString(KEY_PUBLIC, null); }

	public String getPrivateKey(){ return  pref.getString(KEY_PRIVATE, null); }

	public String getKeyGustos(){ return  pref.getString(KEY_GUSTOS, null); }

	public String getKeyNotifications(){ return  pref.getString(KEY_NOTIFICATIONS, "false"); }

	public double getLatitude(){return Double.valueOf(pref.getString(KEY_LATITUDE, "0.0"));}

	public double getLongitude(){return Double.valueOf(pref.getString(KEY_LONGITUDE, "0.0"));}

	public double getRadius(){return Double.valueOf(pref.getString(KEY_RADIUS, "0.0"));}

	public String getPrinterName(){ return  pref.getString(KEY_PRINTER_NAME, "Printer not found"); }

	public String getPrinterDescription(){ return  pref.getString(KEY_PRINTER_DESCRIPTION, "Printer not found"); }

	public boolean getIsDesigner(){ return  pref.getBoolean(KEY_IS_DESIGNER, false); }



	@SuppressWarnings("static-access")
	public String getMW(){
		WifiManager wifiManager = (WifiManager) _context.getSystemService(_context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		String macAddress = wInfo.getMacAddress();
		return macAddress;
	}

	public void setPublickey(String pk)
	{
		editor.putString(KEY_PUBLIC, pk);
		editor.commit();
	}

	public void setPrivateKey(String pk)
	{
		editor.putString(KEY_PRIVATE, pk);
		editor.commit();
	}


	public void setUser(String l)
	{
		editor.putString(KEY_USERLOGIN, l);
		editor.commit();
	}

	public void setEmail(String l)
	{
		editor.putString(KEY_EMAIL, l);
		editor.commit();
	}

	public void setImageProfile(String l)
	{
		editor.putString(KEY_IMAGEPROFILE, l);
		editor.commit();
	}

	public void setPrinterImage(String l){
		editor.putString(KEY_PRINTERIMAGE, l);
		editor.commit();
	}

	public void setKeyGustos(String l)
	{
		editor.putString(KEY_GUSTOS, l);
		editor.commit();
	}

	public void setKeyNotifications(String n)
	{
		editor.putString(KEY_NOTIFICATIONS, n);
		editor.commit();
	}

	public void setPassword(String l)
	{
		editor.putString(KEY_PASSWORD, l);
		editor.commit();
	}


	public void setTMediaPath(String l)
	{
		editor.putString(KEY_MEDIAPATH, l);
		editor.commit();
	}


	public void setServer(String l)
	{
		editor.putString(KEY_SERVER, l);
		editor.commit();
	}
	public void setWorking(String l)
	{
		editor.putString(KEY_WORKING, l);
		editor.commit();
	}

	public void setIsPrinter(Boolean b){
		editor.putBoolean(KEY_ISPRINTER, b);
		editor.commit();
	}

	public void setLongitude(double d){
        editor.putString(KEY_LONGITUDE, String.valueOf(d));
        editor.commit();
    }
    public void setLatitude(double d){
        editor.putString(KEY_LATITUDE, String.valueOf(d));
        editor.commit();
    }
    public void setRadius(double d){
        editor.putString(KEY_RADIUS, String.valueOf(d));
        editor.commit();
    }

    public void setPrinterName(String s){
		editor.putString(KEY_PRINTER_NAME, s);
		editor.commit();
	}

	public void setPrinterDescription(String s){
		editor.putString(KEY_PRINTER_DESCRIPTION, s);
		editor.commit();
	}

	public void setIsDesigner(boolean s){
		editor.putBoolean(KEY_IS_DESIGNER, s);
		editor.commit();
	}

	public void setLogin()
	{
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(KEY_PIRACY, getMW());
		editor.commit();
	}

	public void logout() {

		editor.clear();
		editor.putBoolean(IS_LOGIN, false);
		editor.commit();

	}

}
