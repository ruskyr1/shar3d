package com.sdm.shar3d.Services;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Edgar on 01/04/2017.
 */

public class RequestService {

    private static RequestService instance;
    private static Context context;
    private RequestQueue rQueue;

    private RequestService(Context context){
        this.context = context;
        rQueue = getRequestQueue();
    }

    public static synchronized RequestService getInstance(Context context) {
        if (instance == null) {
            instance = new RequestService(context);
        }
        return instance;
    }



    public RequestQueue getRequestQueue() {
        if (rQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            rQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return rQueue;
    }

    public <T> void addToQueue(Request<T> req){
        getRequestQueue().add(req);
    }


}