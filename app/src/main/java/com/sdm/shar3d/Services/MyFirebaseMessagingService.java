/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sdm.shar3d.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sdm.shar3d.Activities.MainActivity;
import com.sdm.shar3d.Fragments.FragmentAnuncios;
import com.sdm.shar3d.Helpers.Notificacion;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        Log.d(TAG, "NOTIFICACION:");
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "getTtl: " + remoteMessage.getTtl());
        Log.d(TAG, "getMessageId: " + remoteMessage.getMessageId());
        Log.d(TAG, "getCollapse: " + remoteMessage.getCollapseKey());
        Log.d(TAG, "getTo: " + remoteMessage.getTo());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "Message Notification Icon: " + remoteMessage.getNotification().getIcon());
            Log.d(TAG, "Message Notification Tag: " + remoteMessage.getNotification().getTag());

        }

        sendNotification(new Notificacion(
                                    remoteMessage.getNotification().getTitle(),
                                    remoteMessage.getNotification().getBody(),
                                    remoteMessage.getNotification().getIcon(),
                                    remoteMessage.getData() ));

        sendNotificationBroadcast(remoteMessage);
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]



//http://www.hermosaprogramacion.com/2016/06/android-push-notifications-firebase-cloud-messaging/
    //http://www.androidhive.info/2012/10/android-push-notifications-using-google-cloud-messaging-gcm-php-and-mysql/

    private void sendNotificationBroadcast(RemoteMessage remoteMessage) {
        Log.d(TAG, "sendBroadcast: " + remoteMessage.getData());

        Intent intent;

        Log.d(TAG, "ES ANUNCIO " + remoteMessage.getData().toString());

        intent = new Intent(FragmentAnuncios.ACTION_NOTIFY_NEW_ANUNCIO);
        intent.putExtra("anuncio",remoteMessage.getData().get("anuncio"));



        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(intent);
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notificacion FCM notificaction received.
     */
    private void sendNotification(Notificacion notificacion) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //Aqui le digo a la actividad que fragment debe abrir si pulsas sobre la notificacion
        intent.putExtra("notification",notificacion.getIcon());

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getApplicationContext().getResources().getIdentifier(notificacion.getIcon(), "drawable", getApplicationContext().getPackageName()))
                .setContentTitle(notificacion.getTitulo())
                .setContentText(notificacion.getCuerpo())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }
}