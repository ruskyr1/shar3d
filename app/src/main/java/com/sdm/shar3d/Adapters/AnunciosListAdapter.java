package com.sdm.shar3d.Adapters;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sdm.shar3d.Helpers.CircleTransform;
import com.sdm.shar3d.Helpers.SparseBooleanArrayParcelable;
import com.sdm.shar3d.Helpers.Utils;
import com.sdm.shar3d.Persistency.SharedPreferencesHelper;
import com.sdm.shar3d.R;
import com.sdm.shar3d.REST.Anuncio;

import java.util.ArrayList;
import java.util.Collections;


@SuppressWarnings("rawtypes")
public class AnunciosListAdapter
        extends RecyclerView.Adapter<AnunciosListAdapter.AnuncioViewHolder>
        implements View.OnClickListener {

    private ArrayList<Anuncio> anuncios = new ArrayList<>();
    private View.OnClickListener listener;
    private SparseBooleanArrayParcelable mSelectedPositions = new SparseBooleanArrayParcelable();
    private Context ctx;


    public AnunciosListAdapter(Context _ctx, ArrayList<Anuncio> _anuncios) {
        this.ctx = _ctx;
        this.anuncios = _anuncios;
    }

    //inicializa la vista holder de los elementos
    @Override
    public AnuncioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_cardview_anuncios, parent, false);

        itemView.setOnClickListener(this);
        //donde se gestiona la funcionalidad de cada item
        AnuncioViewHolder avh = new AnuncioViewHolder(itemView);

        return avh;
    }

    //rellena cada uno de los items del dataset cuando esta preparado
    @Override
    public void onBindViewHolder(AnuncioViewHolder holder, int position) {
        Anuncio item = anuncios.get(position);
        holder.bindAnuncio(item);
    }

    @Override
    public int getItemCount() {
        return anuncios.size();
    }

    @Override
    public void onClick(View v) {

        if (listener != null)
            listener.onClick(v);
    }


    public void resetStatusTask() {
        for (int i = 0; i < anuncios.size(); i++) {
            mSelectedPositions.put(i, false);
        }
    }

    public SparseBooleanArrayParcelable getmStatusTaskSelected() {
        return mSelectedPositions;
    }



    public ArrayList<Integer> getSelectedPositions() {
        ArrayList<Integer> result = new ArrayList<>();
        for (int p = 0; p < mSelectedPositions.size(); p++)
            if (mSelectedPositions.get(p)) result.add(p);
        return result;
    }

    public ArrayList<Anuncio> getSelectedElements() {
        ArrayList<Anuncio> result = new ArrayList<>();
        for (Integer p : getSelectedPositions())
            result.add(anuncios.get(p));
        return result;
    }


    public void setOnClickListener(View.OnClickListener listener) {

        this.listener = listener;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class AnuncioViewHolder
            extends RecyclerView.ViewHolder {
        //atributos de cada uno de los items en la ui
        private TextView nombre;
        private TextView lugar;
        private ImageView imagenImp;
        private View itemView;

        public AnuncioViewHolder(View _itemView) {
            super(_itemView);
            //inicializo punteros
            itemView = _itemView;
            nombre = (TextView) itemView.findViewById(R.id.nombreView);
            lugar = (TextView) itemView.findViewById(R.id.lugView);
            imagenImp = (ImageView) itemView.findViewById(R.id.imagenImpresora);
        }

        //relleno item
        public void bindAnuncio(Anuncio e) {
            SharedPreferencesHelper session = new SharedPreferencesHelper(ctx);
            if (e.getImgPrinter() != null && !Utils.isNullOrEmpty(e.getImgPrinter()))
                Glide.with(ctx).load(Base64.decode(e.getImgPrinter(), Base64.DEFAULT))
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new CircleTransform(ctx))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imagenImp);


            nombre.setText(e.getPrintername() + "\n" + e.getUsername());
            if (e.getLatitude() != 0.0 && e.getLongitude() != 0.0 && session.getLatitude() != 0.0 && session.getLongitude() != 0.0) {
                lugar.setText(Utils.calculateDistance(e.getLatitude(), e.getLongitude(), session.getLatitude(), session.getLongitude()));
            } else
                lugar.setText(e.getZona());
        }


        public ImageView getImagen() {
            return imagenImp;
        }


    }
}