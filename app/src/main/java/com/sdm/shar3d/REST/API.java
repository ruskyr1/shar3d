package com.sdm.shar3d.REST;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;



public interface API {
    public static final String APIKEY = "78uYSF-3hn2cshyPy8tECye7XcNtf4Ma";
    //endpoints

    @GET("/tokens")
    Call<TokensResponse> obtenerTokens();

    @POST("/tokens")
    Call<TokenResponse> registerNotificationToken(@Body HashMap<String, String> token);

    @DELETE("/tokens/{idtoken}")
    Call<ResponseBody> deleteNotificationToken(@Path("idtoken") String idtoken);

    @GET("/api/1/databases/sdm/collections/anuncios?apiKey=" + APIKEY)
    Call<List<Anuncio>> obtenerAnuncios();

    @POST("/api/1/databases/sdm/collections/anuncios?apiKey=" + APIKEY)
    Call<AnuncioResponse> publicaAnuncio(@Body Anuncio anun);


}