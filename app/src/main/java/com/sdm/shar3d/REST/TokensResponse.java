
package com.sdm.shar3d.REST;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class TokensResponse {

    @SerializedName("exito")
    @Expose
    private String exito;
    @SerializedName("tokens")
    @Expose
    private List<Token> tokens = new ArrayList<Token>();



    /**
     *
     * @return
     *     The exito
     */
    public String getExito() {
        return exito;
    }

    /**
     *
     * @param exito
     *     The exito
     */
    public void setExito(String exito) {
        this.exito = exito;
    }
    /**
     *
     * @return
     *     The tokens
     */
    public List<Token> getTokens() {
        return tokens;
    }

    /**
     *
     * @param ofertas
     *     The ofertas
     */
    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

}
