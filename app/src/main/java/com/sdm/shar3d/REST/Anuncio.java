
package com.sdm.shar3d.REST;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Anuncio implements Parcelable {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("printername")
    @Expose
    private String printername;
    @SerializedName("imgPrinter")
    @Expose
    private String imgPrinter;
    @SerializedName("imgUser")
    @Expose
    private String imgUser;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("zona")
    @Expose
    private String zona;
    @SerializedName("contacto")
    @Expose
    private String contacto;
    @SerializedName("precio")
    @Expose
    private String precio;
    @SerializedName("descPrinter")
    @Expose
    private String descPrinter;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("_id")
    @Expose
    private Object _id;

    public String get_id() {
        String id = "";
        try {
            id = new JSONObject(this._id.toString()).getString("$oid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrintername() {
        return printername;
    }

    public void setPrintername(String printername) {
        this.printername = printername;
    }

    public String getImgPrinter() {
        return imgPrinter;
    }

    public void setImgPrinter(String imgPrinter) {
        this.imgPrinter = imgPrinter;
    }

    public String getImgUser() {
        return imgUser;
    }

    public void setImgUser(String imgUser) {
        this.imgUser = imgUser;
    }

    public String getDescPrinter() {
        return descPrinter;
    }

    public void setDescPrinter(String descPrinter) {
        this.descPrinter = descPrinter;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public static final Creator<Anuncio> CREATOR = new Creator<Anuncio>() {
        @Override
        public Anuncio createFromParcel(Parcel in) {
            return new Anuncio(in);
        }

        @Override
        public Anuncio[] newArray(int size) {
            return new Anuncio[size];
        }
    };




    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }


    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Anuncio() {
    }

    public Anuncio(String username, String printername, String imgprinter, String imguser, String descripcion, String zona, String contacto, String precio, String descprinter, double latitude, double longitude) {
        this.username = username;
        this.printername = printername;
        this.imgPrinter = imgprinter;
        this.imgUser = imguser;
        this.descripcion = descripcion;
        this.zona = zona;
        this.contacto = contacto;
        this.precio = precio;
        this.descPrinter = descprinter;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Anuncio(Parcel in) {
        username = in.readString();
        printername = in.readString();
        imgPrinter = in.readString();
        imgUser = in.readString();
        descripcion = in.readString();
        zona = in.readString();
        contacto = in.readString();
        precio = in.readString();
        descPrinter = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(printername);
        dest.writeString(imgPrinter);
        dest.writeString(imgUser);
        dest.writeString(descripcion);
        dest.writeString(zona);
        dest.writeString(contacto);
        dest.writeString(precio);
        dest.writeString(descPrinter);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }


}
