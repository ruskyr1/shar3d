
package com.sdm.shar3d.REST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AnuncioResponse {

    @SerializedName("exito")
    @Expose
    private String exito;
    @SerializedName("anuncio")
    @Expose
    private Anuncio anuncio;



    /**
     *
     * @return
     *     The exito
     */
    public String getExito() {
        return exito;
    }

    /**
     *
     * @param exito
     *     The exito
     */
    public void setExito(String exito) {
        this.exito = exito;
    }
    /**
     *
     * @return
     *     The tokens
     */
    public Anuncio getAnuncio() {
        return anuncio;
    }

    /**
     *
     * @param ofertas
     *     The ofertas
     */
    public void setAnuncio(Anuncio anun) {
        this.anuncio = anun;
    }

}
