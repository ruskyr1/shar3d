
package com.sdm.shar3d.REST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class AnunciosResponse {

    @SerializedName("exito")
    @Expose
    private String exito;
    @SerializedName("anuncios")
    @Expose
    private List<Anuncio> anuncios = new ArrayList<Anuncio>();

    /**
     * 
     * @return
     *     The exito
     */
    public String getExito() {
        return exito;
    }

    /**
     * 
     * @param exito
     *     The exito
     */
    public void setExito(String exito) {
        this.exito = exito;
    }

    /**
     * 
     * @return
     *     The ofertas
     */
    public List<Anuncio> getAnuncios() {
        return anuncios;
    }

    /**
     * 
     * @param ofertas
     *     The ofertas
     */
    public void setAnuncios(List<Anuncio> anuncios) {
        this.anuncios = anuncios;
    }

}
