package com.sdm.shar3d.REST;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestClient {
    private static final String BASE_URL = "https://api.mlab.com/";
    private static API REST_CLIENT;

    static {
        setupRestClient();
    }

    private RestClient(){}

    public static API getApiService(){ return REST_CLIENT; }

    private static void setupRestClient(){
        System.out.println("entraAAAAA");

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor .Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().readTimeout(100, TimeUnit.SECONDS).connectTimeout(100, TimeUnit.SECONDS);

         httpClient.addInterceptor(logging);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(BASE_URL)

                        .addConverterFactory(GsonConverterFactory.create());


        Retrofit restAdapter = builder.client(httpClient.build()).build();
        REST_CLIENT = restAdapter.create(API.class);
        System.out.println("entraAAAAA2");

    }
}
