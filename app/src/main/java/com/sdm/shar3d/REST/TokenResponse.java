
package com.sdm.shar3d.REST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class TokenResponse {

    @SerializedName("exito")
    @Expose
    private String exito;
    @SerializedName("token")
    @Expose
    private Token insertedToken;



    /**
     *
     * @return
     *     The exito
     */
    public String getExito() {
        return exito;
    }

    /**
     *
     * @param exito
     *     The exito
     */
    public void setExito(String exito) {
        this.exito = exito;
    }
    /**
     *
     * @return
     *     The tokens
     */
    public Token getInsertedToken() {
        return insertedToken;
    }

    /**
     *
     * @param ofertas
     *     The ofertas
     */
    public void setInsertedToken(Token newToken) {
        this.insertedToken = newToken;
    }

}
